﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SystemAnkietPJWSTK.DAL
{
    public class OsobaDbContext : IOsobaDbService
    {
        
        public IEnumerable<Osoba> GetOsobaByWydzial(int IdWydzial)
        {
            List<Osoba> osoby = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Osoba_By_Wydzial", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idWydzial", IdWydzial);
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        osoby = new List<Osoba>();
                        Osoba osoba = null;
                        while (dr.Read())
                        {
                            osoba = new Osoba();
                            osoba.IdOsoba = (int)dr["idOsoba"];
                            osoby.Add(osoba);
                        }
                    }

                    return osoby;
                }
            }
            catch (Exception exc)
            {
                return osoby;
            }
        }

        public IEnumerable<Osoba> GetOsobaByStudia(int IdStudia)
        {
            List<Osoba> osoby = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Osoba_By_Studia", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idStudia", IdStudia);
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        osoby = new List<Osoba>();
                        Osoba osoba = null;
                        while (dr.Read())
                        {
                            osoba = new Osoba();
                            osoba.IdOsoba = (int)dr["idOsoba"];
                            osoby.Add(osoba);
                        }
                    }

                    return osoby;
                }
            }
            catch (Exception exc)
            {
                return osoby;
            }
        }

        public IEnumerable<Osoba> GetOsobaByGrupaStudencka(int IdGrupa)
        {
            List<Osoba> osoby = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Osoba_By_Grupa_Studencka", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idGrupa", IdGrupa);
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        osoby = new List<Osoba>();
                        Osoba osoba = null;
                        while (dr.Read())
                        {
                            osoba = new Osoba();
                            osoba.IdOsoba = (int)dr["idOsoba"];
                            osoby.Add(osoba);
                        }
                    }

                    return osoby;
                }
            }
            catch (Exception exc)
            {
                return osoby;
            }
        }

        public IEnumerable<Osoba> GetOsobaByAnkieta(int idAnkieta)
        {
            List<Osoba> osoby = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Osoba_By_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.VarChar).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        osoby = new List<Osoba>();
                        Osoba osoba = null;
                        while (dr.Read())
                        {
                            osoba = new Osoba();                         
                            osoba.IdOsoba = (int)dr["IdOsoba"];
                            osoba.KontoAD = dr["Konto_AD"].ToString();
                            osoba.Imie = dr["Imie1"].ToString();
                            osoba.Nazwisko = dr["Nazwisko"].ToString();
                            osoba.Email = dr["E_Mail1"].ToString();
                            osoby.Add(osoba);
                        }
                    }

                    return osoby;
                }
            }
            catch (Exception exc)
            {
                return osoby;
            }
        }

        public IEnumerable<Osoba> GetOsobaITNByStudia(int idStudia)
        {
            List<Osoba> osoby = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Osoba_ITN_By_Studia", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdStudia", SqlDbType.Int).Value = idStudia;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        osoby = new List<Osoba>();
                        Osoba osoba = null;
                        while (dr.Read())
                        {
                            osoba = new Osoba();
                            osoba.IdOsoba = (int)dr["IdOsoba"];
                            osoba.Imie = dr["Imie1"].ToString();
                            osoba.Nazwisko = dr["Nazwisko"].ToString();
                            osoba.Email = dr["E_Mail1"].ToString();
                            osoby.Add(osoba);
                        }
                    }

                    return osoby;
                }
            }
            catch (Exception exc)
            {
                return osoby;
            }
        } 
    }
}