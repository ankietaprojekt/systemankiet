﻿using System;
using System.Collections.Generic;
using SystemAnkietPJWSTK.DAL.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using SystemAnkietPJWSTK.Models;


namespace SystemAnkietPJWSTK.DAL
{
    public class PytanieDbContext : IPytanieDbService
    {

        public int InsertPytanie(Pytanie pytanie)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Insert_Pytanie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    pytanie.MinOdpowiedzi = 1;
                    pytanie.MaxOdpowiedzi = 1;

                    cmd.Parameters.Add("Tresc", SqlDbType.VarChar).Value = pytanie.Tresc;
                    cmd.Parameters.Add("Typ", SqlDbType.Int).Value = pytanie.Typ;
                    cmd.Parameters.Add("MinOdpowiedzi", SqlDbType.Int).Value = pytanie.MinOdpowiedzi;
                    cmd.Parameters.Add("MaxOdpowiedzi", SqlDbType.Int).Value = pytanie.MaxOdpowiedzi;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = pytanie.IdAnkieta;

                    int pytanieId = -1;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                            pytanieId = Convert.ToInt32(dr["scope"]);
                    }

                    return pytanieId;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc);
                return -1;
            }
        }

        public Exception UpdatePytanie(Pytanie pytanie)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Update_Pytanie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    cmd.Parameters.Add("Id", SqlDbType.Int).Value = pytanie.Id;
                    cmd.Parameters.Add("Tresc", SqlDbType.VarChar).Value = pytanie.Tresc;
                    cmd.Parameters.Add("Typ", SqlDbType.Int).Value = pytanie.Typ;
                    cmd.Parameters.Add("MinOdppowiedzi", SqlDbType.Int).Value = pytanie.MinOdpowiedzi;
                    cmd.Parameters.Add("MaxOdpowiedzi", SqlDbType.Int).Value = pytanie.MaxOdpowiedzi;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = pytanie.IdAnkieta;

                    cmd.ExecuteNonQuery();

                    return null;
                }
            }
            catch (Exception exc)
            {
                return exc;
            }
        }

        public Exception DeletePytanie(Pytanie pytanie)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Pytanie_Delete", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    cmd.Parameters.Add("Id", SqlDbType.Int).Value = pytanie.Id;

                    cmd.ExecuteNonQuery();

                    return null;
                }
            }
            catch (Exception exc)
            {
                return exc;
            }
        }

        public IEnumerable<Pytanie> GetPytanieByAnkieta(int idAnkieta)
        {
            List<Pytanie> pytania = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Pytanie_By_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.Int).Value = idAnkieta;

                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        pytania = new List<Pytanie>();
                        Pytanie pt = null;
                        while (dr.Read())
                        {
                            pt = new Pytanie();
                            pt.Id = (int)dr["IdPytanie"];
                            pt.Tresc = dr["Tresc"].ToString();
                            pt.Typ = (int)dr["Typ"];
                            if (dr["MinOdpowiedzi"] != DBNull.Value)
                                pt.MinOdpowiedzi = (int)dr["MinOdpowiedzi"];
                            if (dr["MaxOdpowiedzi"] != DBNull.Value)
                                pt.MaxOdpowiedzi = (int)dr["MaxOdpowiedzi"];
                            pt.IdAnkieta = (int)dr["IdAnkieta"];

                            pytania.Add(pt);
                        }
                    }

                    return pytania;
                }
            }
            catch (Exception exc)
            {
                return pytania;
            }
        }

        public int DeletePytanieByAnkieta(int idAnkieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Delete_Pytanie_By_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();
                    int ret = cmd.ExecuteNonQuery();

                    return ret;
                }                
            }
            catch (Exception e)
            {
                return -1;
            }
        }
    }
}