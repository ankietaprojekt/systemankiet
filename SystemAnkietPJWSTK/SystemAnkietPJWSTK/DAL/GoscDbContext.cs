﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SystemAnkietPJWSTK.DAL
{
    public class GoscDbContext : IGoscDbService
    {
        public IEnumerable<Gosc> GetGoscByAnkieta(int idAnkieta)
        {
            List<Gosc> osoby = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Gosc_By_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        osoby = new List<Gosc>();
                        Gosc osoba = null;
                        while (dr.Read())
                        {
                            osoba = new Gosc();
                            osoba.IdGosc = (int)dr["IdGosc"];
                            osoba.Token = dr["Link"].ToString();
                            osoba.Email = dr["Email"].ToString();

                            osoby.Add(osoba);
                        }
                    }

                    return osoby;
                }
            }
            catch (Exception exc)
            {
                return osoby;
            }
        }
    }
}