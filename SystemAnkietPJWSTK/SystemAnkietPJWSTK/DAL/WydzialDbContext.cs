﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SystemAnkietPJWSTK.DAL
{
    public class WydzialDbContext : IWydzialDbService
    {
        public IEnumerable<Wydzial> SelectWydzialy()
        {
            List<Wydzial> wydzialy = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("rek_Wydzialy_sel", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        wydzialy = new List<Wydzial>();
                        Wydzial wydzial = null;

                        while (dr.Read())
                        {
                            wydzial = new Wydzial();
                            wydzial.IdWydzial = (int)dr["IdWydzial"];
                            wydzial.Nazwa = dr["Nazwa"].ToString();

                            wydzialy.Add(wydzial);
                        }
                    }

                    return wydzialy;
                }
            }
            catch (Exception exc)
            {
                return wydzialy;
            }
        }

        public bool AddWydzialToAnkieta(int idWydzial, Ankieta ankieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_Wydzial_To_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idWydzial", SqlDbType.Int).Value = idWydzial ;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.Int).Value = ankieta.IdAnkieta;

                    con.Open();
                    cmd.ExecuteReader();
                    return true;

                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<Wydzial> GetWydzialByAnkieta(int idAnkieta)
        {
            List<Wydzial> wydzialy = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Wydzial_By_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        wydzialy = new List<Wydzial>();
                        Wydzial wydzial = null;

                        while (dr.Read())
                        {
                            wydzial = new Wydzial();
                            wydzial.IdWydzial = (int)dr["IdWydzial"];
                            wydzial.Nazwa = dr["Nazwa"].ToString();

                            wydzialy.Add(wydzial);
                        }
                    }

                    return wydzialy;
                }
            }
            catch (Exception exc)
            {
                return wydzialy;
            }
        }
    }
}