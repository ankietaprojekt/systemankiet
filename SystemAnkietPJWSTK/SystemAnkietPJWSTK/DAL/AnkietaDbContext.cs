﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using SystemAnkietPJWSTK.Utils;


namespace SystemAnkietPJWSTK.DAL
{
    public class AnkietaDbContext : IAnkietaDbService
    {
        public IEnumerable<Ankieta> GetAnkietaByCreator(int userID)
        {
            List<Ankieta> ankiety = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Ankieta_By_Creator", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdOsoba", SqlDbType.Int).Value = userID;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        ankiety = new List<Ankieta>();
                        Ankieta ankieta = null;

                        while (dr.Read())
                        {
                            ankieta = new Ankieta();
                            ankieta.IdAnkieta = (int)dr["IdAnkieta"];
                            ankieta.DataUtw = (DateTime)dr["DataUtw"];
                            ankieta.DataRozp = (DateTime)dr["DataRozp"];
                            ankieta.DataZam = (DateTime)dr["DataZam"];
                            ankieta.DataMod = (DateTime)dr["DataMod"];
                            ankieta.Nazwa = dr["Nazwa"].ToString();
                            ankieta.Opis = dr["Opis"].ToString();
                            ankieta.TrescMaila = dr["TrescMaila"].ToString();
                            ankieta.CzyEdyt = (bool)dr["CzyEdyt"];
                            ankieta.CzyAnonim = (bool)dr["CzyAnonim"];
                            try
                            {
                                ankieta.CzasPrzyp = (DateTime)dr["CzasPrzyp"];
                                ankieta.CzyPrzypomnienie = true;
                            }
                            catch (Exception nullExp)
                            {
                                ankieta.CzasPrzyp = null;
                                ankieta.CzyPrzypomnienie = false;
                            }

                            ankieta.Status = dr["Status"].ToString();
                            Dictionary<String, Int32> statistics = GetAnkietaStatistics(ankieta.IdAnkieta);
                            ankieta.IloscOdpowiedzi = statistics["iloscOdpowiedzi"];
                            ankieta.IloscRespondentow = statistics["iloscRespondentow"];

                            ankiety.Add(ankieta);
                        }
                    }

                    return ankiety;
                }
            }
            catch (Exception exc)
            {
                Log.d("GetAnkietaByUser e: " + exc.ToString());
                return ankiety;
            }
        }

        public IEnumerable<Ankieta> GetAnkietaRoboczaByCreator(int userID)
        {
            List<Ankieta> ankiety = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Ankieta_Robocza_By_Creator", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdOsoba", SqlDbType.Int).Value = userID;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        ankiety = new List<Ankieta>();
                        Ankieta ankieta = null;

                        while (dr.Read())
                        {
                            ankieta = new Ankieta();
                            ankieta.IdAnkieta = (int)dr["IdAnkieta"];
                            ankieta.DataUtw = (DateTime)dr["DataUtw"];
                            ankieta.DataRozp = (DateTime)dr["DataRozp"];
                            ankieta.DataZam = (DateTime)dr["DataZam"];
                            ankieta.DataMod = (DateTime)dr["DataMod"];
                            ankieta.Nazwa = dr["Nazwa"].ToString();
                            ankieta.Opis = dr["Opis"].ToString();
                            ankieta.TrescMaila = dr["TrescMaila"].ToString();
                            ankieta.CzyEdyt = (bool)dr["CzyEdyt"];
                            ankieta.CzyAnonim = (bool)dr["CzyAnonim"];
                            try
                            {
                                ankieta.CzasPrzyp = (DateTime)dr["CzasPrzyp"];
                                ankieta.CzyPrzypomnienie = true;
                            }
                            catch (Exception nullExp)
                            {
                                ankieta.CzasPrzyp = null;
                                ankieta.CzyPrzypomnienie = false;
                            }
                            ankieta.Status = dr["Status"].ToString();

                            ankiety.Add(ankieta);
                        }
                    }

                    return ankiety;
                }
            }
            catch (Exception exc)
            {
                return ankiety;
            }
        }

        public Dictionary<String, Int32> GetAnkietaStatistics(int ankietaID)
        {
            Dictionary<String, Int32> dict = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Ankieta_Statistics", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = ankietaID;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        dict = new Dictionary<String, Int32>();

                        while (dr.Read())
                        {
                            dict.Add("iloscOdpowiedzi", (int)dr["iloscOdpowiedzi"]);
                            dict.Add("iloscRespondentow", (int)dr["iloscRespondentow"]);
                        }
                    }

                }

                return dict;
            }
            catch (Exception exc)
            {
                return dict;
            }
        }

        public int UpdateAnkieta(Ankieta ankieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Update_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("ID", SqlDbType.Int).Value = ankieta.IdAnkieta;
                    cmd.Parameters.Add("DatRozp", SqlDbType.DateTime).Value = ankieta.DataRozp;
                    cmd.Parameters.Add("DatZam", SqlDbType.DateTime).Value = ankieta.DataZam;
                    cmd.Parameters.Add("DatMod", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("Naz", SqlDbType.VarChar).Value = ankieta.Nazwa;
                    cmd.Parameters.Add("Op", SqlDbType.VarChar).Value = ankieta.Opis;
                    cmd.Parameters.Add("TrescMail", SqlDbType.VarChar).Value = ankieta.TrescMaila;
                    cmd.Parameters.Add("CzyEd", SqlDbType.Bit).Value = ankieta.CzyEdyt;
                    cmd.Parameters.Add("CzasPrzy", SqlDbType.DateTime).Value = ankieta.CzasPrzyp;
                    cmd.Parameters.Add("CzyAn", SqlDbType.Bit).Value = ankieta.CzyAnonim;
                    cmd.Parameters.Add("Status", SqlDbType.Int).Value = GetStatusIdByName(ankieta.Status);

                    con.Open();

                    int ret = cmd.ExecuteNonQuery();

                    return ret;
                }
            }
            catch (Exception e)
            {
                Log.d(e.ToString());
                return -1;
            }
        }

        public int GetStatusIdByName(string statusName)
        {
            int statusID = 0;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Status_ID_By_Name", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("statusName", SqlDbType.VarChar).Value = statusName;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            statusID = (int)dr["IdStatus"];
                        }
                    }

                    return statusID;
                }
            }
            catch (Exception exc)
            {
                return statusID;
            }
        }

        public int InsertAnkieta(Ankieta ankieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Insert_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idOsoba", SqlDbType.Int).Value = ankieta.IdOsoba;
                    cmd.Parameters.Add("dataRozp", SqlDbType.DateTime).Value = ankieta.DataRozp;
                    cmd.Parameters.Add("dataZam", SqlDbType.DateTime).Value = ankieta.DataZam;
                    cmd.Parameters.Add("nazwa", SqlDbType.VarChar).Value = ankieta.Nazwa;
                    cmd.Parameters.Add("opis", SqlDbType.VarChar).Value = ankieta.Opis;
                    cmd.Parameters.Add("trescMail", SqlDbType.VarChar).Value = ankieta.TrescMaila;
                    cmd.Parameters.Add("czyEdyt", SqlDbType.Bit).Value = ankieta.CzyEdyt;
                    cmd.Parameters.Add("czasPrzyp", SqlDbType.DateTime).Value = ankieta.CzasPrzyp;
                    cmd.Parameters.Add("czyAnonim", SqlDbType.Bit).Value = ankieta.CzyAnonim;
                    cmd.Parameters.Add("status", SqlDbType.Int).Value = GetStatusIdByName(ankieta.Status);

                    con.Open();

                    int ankietaId = -1;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ankietaId = Convert.ToInt32(dr["scope"]);
                        }
                    }

                    return ankietaId;
                }
            }
            catch (Exception e)
            {
                Log.d("InsertAnkieta Error: " + e.ToString());
                return -1;
            }
        }

        public IEnumerable<Ankieta> GetAnkietaByOsoba(int userID)
        {
            List<Ankieta> ankiety = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Ankieta_By_Osoba", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("OsobaId", SqlDbType.Int).Value = userID;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    using (var dr = cmd.ExecuteReader())
                    {
                        ankiety = new List<Ankieta>();
                        Ankieta ankieta = null;

                        while (dr.Read())
                        {
                            ankieta = new Ankieta();
                            ankieta.IdAnkieta = (int)dr["IdAnkieta"];
                            ankieta.DataUtw = (DateTime)dr["DataUtw"];
                            ankieta.DataRozp = (DateTime)dr["DataRozp"];
                            ankieta.DataZam = (DateTime)dr["DataZam"];
                            ankieta.DataMod = (DateTime)dr["DataMod"];
                            ankieta.Nazwa = dr["Nazwa"].ToString();
                            ankieta.Opis = dr["Opis"].ToString();
                            ankieta.TrescMaila = dr["TrescMaila"].ToString();
                            ankieta.CzyEdyt = (bool)dr["CzyEdyt"];
                            ankieta.CzyAnonim = (bool)dr["CzyAnonim"];
                            try
                            {
                                ankieta.CzasPrzyp = (DateTime)dr["CzasPrzyp"];
                                ankieta.CzyPrzypomnienie = true;
                            }
                            catch (Exception nullExp)
                            {
                                ankieta.CzasPrzyp = null;
                                ankieta.CzyPrzypomnienie = false;
                            }
                            ankieta.Status = dr["Status"].ToString();

                            Dictionary<String, Int32> statistics = GetAnkietaStatistics(ankieta.IdAnkieta);
                            ankieta.IloscOdpowiedzi = statistics["iloscOdpowiedzi"];
                            ankieta.IloscRespondentow = statistics["iloscRespondentow"];

                            if ((GetStatusIdByName(ankieta.Status) != 3) || (!ankieta.CzyEdyt && IsUserWypelnienieExists(ankieta.IdAnkieta, userID)))
                                continue;

                            ankiety.Add(ankieta);
                        }
                    }

                    return ankiety;
                }
            }
            catch (Exception exc)
            {
                return ankiety;
            }
        }

        public bool IsUserWypelnienieExists(int ankietaID, int userID)
        {
            bool isExists = false;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Is_User_Wypelnienie_Exists", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.VarChar).Value = ankietaID;
                    cmd.Parameters.Add("idOsoba", SqlDbType.VarChar).Value = userID;

                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            isExists = (bool)dr["isExists"];
                        }
                    }

                    return isExists;
                }
            }
            catch (Exception e)
            {
                return isExists;
            }
        }

        public bool IsGuestWypelnienieExists(int ankietaID, string token)
        {
            bool isExists = false;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Is_Guest_Wypelnienie_Exists", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idAnkiety", SqlDbType.VarChar).Value = ankietaID;
                    cmd.Parameters.Add("token", SqlDbType.VarChar).Value = token;

                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            isExists = (bool)dr["isExists"];
                        }
                    }

                    return isExists;
                }
            }
            catch (Exception e)
            {
                return isExists;
            }
        }

        public Ankieta GetAnkietaByToken(string token)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Ankieta_By_Token", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("token", SqlDbType.VarChar).Value = token;

                    con.Open();

                    Ankieta ankieta = null;

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ankieta = new Ankieta();
                            ankieta.IdAnkieta = (int)dr["IdAnkieta"];
                            ankieta.DataUtw = (DateTime)dr["DataUtw"];
                            ankieta.DataRozp = (DateTime)dr["DataRozp"];
                            ankieta.DataZam = (DateTime)dr["DataZam"];
                            ankieta.DataMod = (DateTime)dr["DataMod"];
                            ankieta.Nazwa = dr["Nazwa"].ToString();
                            ankieta.Opis = dr["Opis"].ToString();
                            ankieta.TrescMaila = dr["TrescMaila"].ToString();
                            ankieta.CzyEdyt = (bool)dr["CzyEdyt"];
                            ankieta.CzyAnonim = (bool)dr["CzyAnonim"];
                            try
                            {
                                ankieta.CzasPrzyp = (DateTime)dr["CzasPrzyp"];
                                ankieta.CzyPrzypomnienie = true;
                            }
                            catch (Exception nullExp)
                            {
                                ankieta.CzasPrzyp = null;
                                ankieta.CzyPrzypomnienie = false;
                            }
                            ankieta.Status = dr["Status"].ToString();
                            ankieta.IdOsoba = (int)dr["IdOsoba"];

                            if ((GetStatusIdByName(ankieta.Status) != 3) || (!ankieta.CzyEdyt && IsGuestWypelnienieExists(ankieta.IdAnkieta, token)))
                            {
                                return null;
                            }
                        }

                        return ankieta;
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Ankieta GetAnkietaByID(int id)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Ankieta_By_ID", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.VarChar).Value = id;

                    con.Open();

                    Ankieta ankieta = null;

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ankieta = new Ankieta();
                            ankieta.IdAnkieta = (int)dr["IdAnkieta"];
                            ankieta.DataUtw = (DateTime)dr["DataUtw"];
                            ankieta.DataRozp = (DateTime)dr["DataRozp"];
                            ankieta.DataZam = (DateTime)dr["DataZam"];
                            ankieta.DataMod = (DateTime)dr["DataMod"];
                            ankieta.Nazwa = dr["Nazwa"].ToString();
                            ankieta.Opis = dr["Opis"].ToString();
                            ankieta.TrescMaila = dr["TrescMaila"].ToString();
                            ankieta.CzyEdyt = (bool)dr["CzyEdyt"];
                            ankieta.CzyAnonim = (bool)dr["CzyAnonim"];
                            try
                            {
                                ankieta.CzasPrzyp = (DateTime)dr["CzasPrzyp"];
                                ankieta.CzyPrzypomnienie = true;
                            }
                            catch (Exception nullExp)
                            {
                                ankieta.CzasPrzyp = null;
                                ankieta.CzyPrzypomnienie = false;
                            }
                            ankieta.Status = dr["Status"].ToString();
                            Dictionary<String, Int32> statistics = GetAnkietaStatistics(ankieta.IdAnkieta);
                            ankieta.IloscOdpowiedzi = statistics["iloscOdpowiedzi"];
                            ankieta.IloscRespondentow = statistics["iloscRespondentow"];
                            ankieta.IdOsoba = (int)dr["IdOsoba"];
                        }
                    }

                    return ankieta;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool UruchomAnkiete(int idAnkieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Uruchom_Ankiete", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception exc)
            {
                Log.d(exc.ToString());
                return false;
            }
        }

        public bool ZamknijAnkiete(int idAnkieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Zamknij_Ankiete", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception exc)
            {
                Log.d(exc.ToString());
                return false;
            }
        }
    }

    
}