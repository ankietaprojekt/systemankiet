﻿namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IWypelnienieDbService
    {
        int InsertWypelnienie(int ankietaId);
        bool InsertWypGosc(int idWypelnienie, string token);
        bool InsertWypUser(int idWypelnienie, int idUser);
        bool IsUserWypelnienieExist(int idAnkieta, int idUser);
        bool UpdateWypelnienie(int idWypelnienia);
        int GetWypelnienieByUserAnkieta(int idUser, int idAnkieta);
        int GetWypelnienieByGuestAnkieta(string token, int idAnkieta);
    }
}
