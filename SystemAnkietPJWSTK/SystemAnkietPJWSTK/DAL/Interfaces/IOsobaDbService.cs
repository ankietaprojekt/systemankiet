﻿using SystemAnkietPJWSTK.Models;
using System.Collections.Generic;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IOsobaDbService
    {
        IEnumerable<Osoba> GetOsobaByWydzial(int IdWydzial);
        IEnumerable<Osoba> GetOsobaByStudia(int IdStudia);
        IEnumerable<Osoba> GetOsobaByGrupaStudencka(int IdGrupa);
        IEnumerable<Osoba> GetOsobaByAnkieta(int idAnkieta);
        IEnumerable<Osoba> GetOsobaITNByStudia(int idStudia);
    }
}
