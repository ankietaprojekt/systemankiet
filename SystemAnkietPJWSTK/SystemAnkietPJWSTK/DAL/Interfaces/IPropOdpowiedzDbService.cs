﻿using System;
using System.Collections.Generic;
using SystemAnkietPJWSTK.Models;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IPropOdpowiedzDbService
    {
        Exception InsertPropOdp(PropOdpowiedz prop);
        IEnumerable<PropOdpowiedz> GetPropOdpByPytanie(int idPytania);
    }
}