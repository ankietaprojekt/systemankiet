﻿using System;
using System.Collections.Generic;
using SystemAnkietPJWSTK.Models;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IPytanieDbService
    {
        int InsertPytanie(Pytanie pytanie);
        Exception UpdatePytanie(Pytanie pytanie);
        Exception DeletePytanie(Pytanie pytanie);
        IEnumerable<Pytanie> GetPytanieByAnkieta(int idAnkieta);
        int DeletePytanieByAnkieta(int idAnkieta);
    }
}
