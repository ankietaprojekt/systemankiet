﻿using System;
using System.Collections.Generic;
using SystemAnkietPJWSTK.Models;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IOdpowiedzDbService
    {
        Exception AddOdpowiedzOtwarta(int wypelnienieId, int pytanieId, string tresc);
        Exception AddOdpowiedzZamknieta(int wypelnienieId, int poleId);
        IEnumerable<Odpowiedz> GetOdpowiedzByUserAnkieta(int idUser, int idAnkieta);
        IEnumerable<Odpowiedz> GetOdpowiedzByGuestAnkieta(string token, int idAnkieta);
        IEnumerable<Odpowiedz> GetOdpowiedzByGuestAnkieta(int idGosc, int idAnkieta);
        bool UpdateOdpowiedzOtwarta(int idWypelnienia, int idPytania, string tresc);
        bool UpdateOdpowiedzZamknieta(int wypelnienieId, int poleId, int idOdpowiedz);
        int GetOdpowiedzByPoleWypelnienie(int idPola, int idWypelnienia);
        bool DeleteOdpowiedzZamknietaByWypelnienie(int idWypelnienia);
        IEnumerable<Odpowiedz> GetOdpowiedzByPytanie(int idPytania);
    }
}