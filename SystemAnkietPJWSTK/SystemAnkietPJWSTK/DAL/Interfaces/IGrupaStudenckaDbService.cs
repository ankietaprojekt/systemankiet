﻿using System.Collections.Generic;
using SystemAnkietPJWSTK.Models;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IGrupaStudenckaDbService
    {
        IEnumerable<GrupaStudencka> GetGrupaStudenckaForCurrentSemester();
        bool AddGrupaStudenckaToAnkieta(int idGrupaStud, Ankieta ankieta);
        IEnumerable<GrupaStudencka> GetGrupaStudenckaByAnkieta(int idAnkieta);
    }
}
