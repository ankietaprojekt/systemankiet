﻿using SystemAnkietPJWSTK.Models;
using System.Collections.Generic;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IWydzialDbService
    {
        IEnumerable<Wydzial> SelectWydzialy();
        bool AddWydzialToAnkieta(int idWydzial, Ankieta ankieta);
        IEnumerable<Wydzial> GetWydzialByAnkieta(int idAnkieta);
    }
}