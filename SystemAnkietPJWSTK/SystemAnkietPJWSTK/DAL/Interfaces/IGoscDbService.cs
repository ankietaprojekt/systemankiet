﻿using SystemAnkietPJWSTK.Models;
using System.Collections.Generic;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IGoscDbService
    {
        IEnumerable<Gosc> GetGoscByAnkieta(int idAnkieta);
    }
}
