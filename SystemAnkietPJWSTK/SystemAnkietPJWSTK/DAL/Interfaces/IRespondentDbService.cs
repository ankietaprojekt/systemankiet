﻿using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IRespondentDbService
    {
        bool AddOsobaToAnkieta(int idOsoba, Ankieta ankieta);
        bool AddGoscToAnkieta(String email, Ankieta ankieta, String link);
        List<Osoba> GetAllRespondents();
        IEnumerable<Osoba> GetRespondentByAnkieta(int idAnkieta);
        int DeleteAllRespondentsByAnkieta(int idAnkieta);
        IEnumerable<RespondentDoRaportu> GetRespondentForRaportByAnkieta(int idAnkieta);
        IEnumerable<RespondentDoRaportu> GetRespondentWithWypelnienieByAnkieta(int idAnkieta);
    }
}
