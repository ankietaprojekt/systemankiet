﻿using SystemAnkietPJWSTK.Models;
using System.Collections.Generic;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface ISemestrDbService
    {
        bool AddSemestrToAnkieta(int nrStuidia, int parentId, Ankieta ankieta);
        IEnumerable<Semestr> GetActiveSemestrNumbersByStudia(int idStudia);
        IEnumerable<Semestr> GetSemestrByAnkieta(int idAnkieta);
    }
}