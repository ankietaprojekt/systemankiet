﻿using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;

namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IAnkietaDbService
    {
        IEnumerable<Ankieta> GetAnkietaByCreator(int userID);
        IEnumerable<Ankieta> GetAnkietaRoboczaByCreator(int userID);
        IEnumerable<Ankieta> GetAnkietaByOsoba(int userID);
        int UpdateAnkieta(Ankieta ankieta);
        int InsertAnkieta(Ankieta ankieta);
        Ankieta GetAnkietaByToken(string token);
        Ankieta GetAnkietaByID(int idAnkieta);

        int GetStatusIdByName(string statusName);
        Dictionary<String, Int32> GetAnkietaStatistics(int ankietaID);
        bool IsUserWypelnienieExists(int ankietaID, int userID);
        bool IsGuestWypelnienieExists(int ankietaID, string token);

        bool UruchomAnkiete(int idAnkieta);
        bool ZamknijAnkiete(int idAnkieta);
    }
}
