﻿using SystemAnkietPJWSTK.Models;
using System.Collections.Generic;


namespace SystemAnkietPJWSTK.DAL.Interfaces
{
    interface IStudiaDbService
    {
        IEnumerable<Studia> SelectStudia();
        bool AddStudiaToAnkieta(int idStudia, Ankieta ankieta);
        IEnumerable<Studia> GetStudiaByAnkieta(int idAnkieta);
        Studia GetStudiaByID(int idStudia);
    }
}
