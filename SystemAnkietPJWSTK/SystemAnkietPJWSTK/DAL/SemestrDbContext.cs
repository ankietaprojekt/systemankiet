﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SystemAnkietPJWSTK.DAL
{
    public class SemestrDbContext : ISemestrDbService
    {
        public bool AddSemestrToAnkieta(int nrSemestr, int parentId, Ankieta ankieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_Semestr_To_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("nrSemestr", SqlDbType.Int).Value = nrSemestr;
                    cmd.Parameters.Add("idStudia", SqlDbType.Int).Value = parentId;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.Int).Value = ankieta.IdAnkieta;

                    con.Open();
                    cmd.ExecuteReader();
                    return true;

                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<Semestr> GetActiveSemestrNumbersByStudia(int idStudia)
        {
            List<Semestr> numery = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Active_Semestr_Number_By_Studia", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdStudia", SqlDbType.Int).Value = idStudia;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        numery = new List<Semestr>();
                        Semestr numer = null;

                        while (dr.Read())
                        {
                            numer = new Semestr();
                            int nr = (int)dr["NrSemestr"];
                            numer.NrSemestru = nr.ToString();
                            numer.IdStudia = idStudia;

                            numery.Add(numer);
                        }
                    }

                    return numery;
                }
            }
            catch (Exception e)
            {
                return numery;
            }
        }

        public IEnumerable<Semestr> GetSemestrByAnkieta(int idAnkieta)
        {
            List<Semestr> numery = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Semestr_By_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        numery = new List<Semestr>();
                        Semestr numer = null;

                        while (dr.Read())
                        {
                            numer = new Semestr();
                            int nr = (int)dr["NrSemestr"];
                            numer.NrSemestru = nr.ToString();
                            numer.IdStudia = (int)dr["IdStudia"];

                            numery.Add(numer);
                        }
                    }

                    return numery;
                }
            }
            catch (Exception e)
            {
                return numery;
            }
        }
    }
}