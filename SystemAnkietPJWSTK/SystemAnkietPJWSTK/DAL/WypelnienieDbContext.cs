﻿using System;
using SystemAnkietPJWSTK.DAL.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace SystemAnkietPJWSTK.DAL
{
    public class WypelnienieDbContext : IWypelnienieDbService
    {
        public int InsertWypelnienie(int IdAnkieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Insert_Wypelnienie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("idAnkiety", IdAnkieta);

                    con.Open();

                    int wypelnienieId = -1;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                            wypelnienieId = Convert.ToInt32(dr["scope"]);
                    }

                    return wypelnienieId;
                }
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public bool InsertWypGosc(int idWypelninie, string token)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Insert_Wyp_Gosc", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idWypelnienie", SqlDbType.Int).Value = idWypelninie;
                    cmd.Parameters.Add("token", SqlDbType.VarChar).Value = token;

                    con.Open();

                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception exc)
            {
                return false;
            }
        }

        public bool InsertWypUser(int idWypelninie, int idUser)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Insert_Wyp_User", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("idWypelnienie", idWypelninie);
                    cmd.Parameters.AddWithValue("idUser", idUser);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception exc)
            {
                return false;
            }
        }

        public bool IsUserWypelnienieExist(int idAnkieta, int idUser)
        {
            bool exists = false;
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Is_User_Wypelnienie_Exists", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.Int).Value = idAnkieta;
                    cmd.Parameters.Add("idOsoba", SqlDbType.Int).Value = idUser;

                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                            exists = (bool)dr["isExists"];
                    }

                    return exists;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.ToString());
                return exists;
            }
        }

        public bool UpdateWypelnienie(int idWypelnienia)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Update_Wypelnienie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdWypelnienia", SqlDbType.Int).Value = idWypelnienia;

                    con.Open();

                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.ToString());
                return false;
            }
        }

        public int GetWypelnienieByUserAnkieta(int idUser, int idAnkieta)
        {
            int idWypelnienia = -1;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Wypelnienie_By_User_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    cmd.Parameters.Add("IdUser", SqlDbType.Int).Value = idUser;

                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                            idWypelnienia = (int)dr["IdWypelnienie"];
                    }

                    return idWypelnienia;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.ToString());
                return idWypelnienia;
            }
        }

        public int GetWypelnienieByGuestAnkieta(string token, int idAnkieta)
        {
            int idWypelnienia = -1;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Wypelnienie_By_Guest_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    cmd.Parameters.Add("Token", SqlDbType.VarChar).Value = token;

                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                            idWypelnienia = (int)dr["IdWypelnienie"];
                    }

                    return idWypelnienia;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.ToString());
                return idWypelnienia;
            }
        }

  
    }


}