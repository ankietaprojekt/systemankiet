﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SystemAnkietPJWSTK.Utils;


namespace SystemAnkietPJWSTK.DAL
{
    public class StudiaDbContext : IStudiaDbService
    {
        public IEnumerable<Studia> SelectStudia()
        {
            List<Studia> studia = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Select_Studia", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        studia = new List<Studia>();
                        Studia stud = null;

                        while (dr.Read())
                        {
                            stud = new Studia();
                            stud.IdStudia = (int)dr["IdStudia"];
                            stud.Nazwa = dr["Nazwa"].ToString();
                            stud.IdWydzial = (int)dr["IdWydzial"];

                            studia.Add(stud);
                        }
                    }

                    return studia;
                }
            }
            catch (Exception exc)
            {
                return studia;
            }
        }

        public bool AddStudiaToAnkieta(int idStudia, Ankieta ankieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_Studia_To_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idStudia", SqlDbType.Int).Value = idStudia;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.Int).Value = ankieta.IdAnkieta;

                    con.Open();
                    cmd.ExecuteReader();
                    return true;

                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<Studia> GetStudiaByAnkieta(int idAnkieta)
        {
            List<Studia> studia = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Studia_By_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        studia = new List<Studia>();
                        Studia stud = null;

                        while (dr.Read())
                        {
                            stud = new Studia();
                            stud.IdStudia = (int)dr["IdStudia"];
                            stud.Nazwa = dr["Nazwa"].ToString();
                            stud.IdWydzial = (int)dr["IdWydzial"];

                            studia.Add(stud);
                        }
                    }

                    return studia;
                }
            }
            catch (Exception exc)
            {
                return studia;
            }
        }

        public Studia GetStudiaByID(int idStudia)
        {
            Studia stud = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Studia_By_ID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdStudia", SqlDbType.Int).Value = idStudia;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            stud = new Studia();
                            stud.IdStudia = (int)dr["IdStudia"];
                            stud.Nazwa = dr["Nazwa"].ToString();
                            stud.IdWydzial = (int)dr["IdWydzial"];

                        }
                    }

                    return stud;
                }
            }
            catch (Exception exc)
            {
                Log.d(exc.ToString());
                return stud;
            }
        }
    }
}