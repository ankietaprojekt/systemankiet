﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SystemAnkietPJWSTK.DAL
{
    public class OdpowiedzDbContext : IOdpowiedzDbService
    {
        public Exception AddOdpowiedzOtwarta(int wypelnienieId, int pytanieId, string tresc)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_Odpowiedz_Otwarta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idWypelnienie", SqlDbType.Int).Value = wypelnienieId;
                    cmd.Parameters.Add("idPytanie", SqlDbType.Int).Value = pytanieId;
                    cmd.Parameters.Add("trescOdpOtwartej", SqlDbType.VarChar).Value = tresc;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return null;
                }
            }
            catch (Exception e)
            {
                return e;
            }
        }

        public Exception AddOdpowiedzZamknieta(int wypelnienieId, int poleId)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_Odpowiedz_Zamknieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idWypelnienie", SqlDbType.Int).Value = wypelnienieId;
                    cmd.Parameters.Add("idPole", SqlDbType.Int).Value = poleId;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return null;
                }
            }
            catch (Exception e)
            {
                return e;
            }
        }

        public IEnumerable<Odpowiedz> GetOdpowiedzByUserAnkieta(int idUser, int idAnkieta)
        {
            List<Odpowiedz> odpowiedzi = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Odpowiedz_By_User_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdUser", SqlDbType.Int).Value = idUser;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        odpowiedzi = new List<Odpowiedz>();
                        Odpowiedz odp = null;

                        while (dr.Read())
                        {
                            odp = new Odpowiedz();

                            odp.IdOdpowiedz = (int)dr["IdOdpowiedz"];
                            odp.IdWypelnienie = (int)dr["IdWypelnienie"];
                            odp.IdPole = (int)dr["IdPole"];
                            odp.IdPytanie = (Int32)dr["IdPytanie"]; 
                            odp.OdpCzasowa = (DateTime)dr["OdpCzasowa"];
                            odp.TrescOdpOtwartej = dr["TrescOdpOtwartej"].ToString();

                            odpowiedzi.Add(odp);
                        }
                    }

                    return odpowiedzi;
                }
            }
            catch (Exception exc)
            {
                return odpowiedzi;
            }
        }

        public IEnumerable<Odpowiedz> GetOdpowiedzByGuestAnkieta(string token, int idAnkieta)
        {
            List<Odpowiedz> odpowiedzi = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Odpowiedz_By_Guest_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Token", SqlDbType.VarChar).Value = token;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        odpowiedzi = new List<Odpowiedz>();
                        Odpowiedz odp = null;

                        while (dr.Read())
                        {
                            odp = new Odpowiedz();

                            odp.IdOdpowiedz = (int)dr["IdOdpowiedz"];
                            odp.IdWypelnienie = (int)dr["IdWypelnienie"];
                            odp.IdPole = (int)dr["IdPole"];
                            odp.IdPytanie = (Int32)dr["IdPytanie"];
                            odp.OdpCzasowa = (DateTime)dr["OdpCzasowa"];
                            odp.TrescOdpOtwartej = dr["TrescOdpOtwartej"].ToString();

                            odpowiedzi.Add(odp);
                        }
                    }

                    return odpowiedzi;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.StackTrace);
                return odpowiedzi;
            }
        }

        public bool UpdateOdpowiedzOtwarta(int idWypelnienia, int idPytania, string tresc)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Update_Odpowiedz_Otwarta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdWypelnienie", SqlDbType.Int).Value = idWypelnienia;
                    cmd.Parameters.Add("IdPytanie", SqlDbType.Int).Value = idPytania;
                    cmd.Parameters.Add("Tresc", SqlDbType.VarChar).Value = tresc;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception e)
            {
                Utils.Log.d(e.StackTrace);
                return false;
            }
        }

        public bool UpdateOdpowiedzZamknieta(int wypelnienieId, int poleId, int idOdpowiedz)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Update_Odpowiedz_Zamknieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdWypelnienie", SqlDbType.Int).Value = wypelnienieId;
                    cmd.Parameters.Add("IdPola", SqlDbType.Int).Value = poleId;
                    cmd.Parameters.Add("IdOdpowiedz", SqlDbType.Int).Value = idOdpowiedz;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception e)
            {
                Utils.Log.d(e.StackTrace);
                return false;
            }
        }

        public int GetOdpowiedzByPoleWypelnienie(int idPola, int idWypelnienia)
        {
            int idOdpowiedz = -1;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Odpowiedz_By_Pole_Wypelnienie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdPola", SqlDbType.Int).Value = idPola;
                    cmd.Parameters.Add("IdWypelnienia", SqlDbType.Int).Value = idWypelnienia;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            idOdpowiedz = (int)dr["IdOdpowiedz"];
                        }
                    }

                    return idOdpowiedz;
                }
            }
            catch (Exception exc)
            {
                Utils.Log.d(exc.StackTrace);
                return idOdpowiedz;
            }
        }

        public bool DeleteOdpowiedzZamknietaByWypelnienie(int idWypelnienia)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Delete_Odpowiedz_Zamknieta_By_Wypelnienie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdWypelnienie", SqlDbType.Int).Value = idWypelnienia;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception e)
            {
                Utils.Log.d(e.ToString());
                return false;
            }
        }

        public IEnumerable<Odpowiedz> GetOdpowiedzByPytanie(int idPytania)
        {
            List<Odpowiedz> odpowiedzi = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Odpowiedz_By_Pytanie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdPytania", SqlDbType.Int).Value = idPytania;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        odpowiedzi = new List<Odpowiedz>();
                        Odpowiedz odp = null;
                        
                        while (dr.Read())
                        {
                            odp = new Odpowiedz();
                            odp.IdWypelnienie = (int)dr["IdWypelnienie"];
                            odp.IdOdpowiedz = (int)dr["IdOdpowiedz"];
                            odp.IdPole = (int)dr["IdPole"];
                            odp.IdPytanie = (int)dr["IdPytanie"];
                            odp.TrescOdpOtwartej = dr["TrescOdpOtwartej"].ToString();

                            odpowiedzi.Add(odp);
                        }
                    }

                    Utils.Log.d("Odpowiedzi count : " + odpowiedzi.Count);

                    return odpowiedzi;
                }
            }
            catch (Exception exc)
            {
                Utils.Log.d(exc.ToString());
                return odpowiedzi;
            }
        }

        public IEnumerable<Odpowiedz> GetOdpowiedzByGuestAnkieta(int idGosc, int idAnkieta)
        {

            List<Odpowiedz> odpowiedzi = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Odpowiedz_By_GuestID_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdGosc", SqlDbType.Int).Value = idGosc;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        odpowiedzi = new List<Odpowiedz>();
                        Odpowiedz odp = null;

                        while (dr.Read())
                        {
                            odp = new Odpowiedz();

                            odp.IdOdpowiedz = (int)dr["IdOdpowiedz"];
                            odp.IdWypelnienie = (int)dr["IdWypelnienie"];
                            odp.IdPole = (int)dr["IdPole"];
                            odp.IdPytanie = (Int32)dr["IdPytanie"];
                            odp.OdpCzasowa = (DateTime)dr["OdpCzasowa"];
                            odp.TrescOdpOtwartej = dr["TrescOdpOtwartej"].ToString();

                            odpowiedzi.Add(odp);
                        }
                    }

                    return odpowiedzi;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc);
                return odpowiedzi;
            }
        }
    }
}