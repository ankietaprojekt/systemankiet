﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace SystemAnkietPJWSTK.DAL
{
    public class PropOdpowiedzDbContext : IPropOdpowiedzDbService
    {
       
        public Exception InsertPropOdp(PropOdpowiedz prop)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Insert_Prop_Odp", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("propTresc", SqlDbType.VarChar).Value = prop.PropTresc;
                    cmd.Parameters.Add("idPytanie", SqlDbType.Int).Value = prop.IdPytanie;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    return null;
                }
            }
            catch (Exception e)
            {
                return e;
            }
        }

        public IEnumerable<PropOdpowiedz> GetPropOdpByPytanie(int IdPytanie)
        {
            List<PropOdpowiedz> propOdpowiedzi = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Prop_Odp_By_Pytanie", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idPytanie", IdPytanie);
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        propOdpowiedzi = new List<PropOdpowiedz>();
                        PropOdpowiedz propOdpowiedz = null;
                        while (dr.Read())
                        {
                            propOdpowiedz = new PropOdpowiedz();
                            propOdpowiedz.IdPole = (int)dr["IdPole"];
                            propOdpowiedz.PropTresc = dr["PropTresc"].ToString();
                            propOdpowiedz.IdPytanie = (int)dr["IdPytanie"];
                            propOdpowiedzi.Add(propOdpowiedz);
                        }
                    }

                    return propOdpowiedzi;
                }
            }
            catch (Exception exc)
            {
                return propOdpowiedzi;
            }
        }
    }
}