﻿using System;
using System.Collections.Generic;
using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace SystemAnkietPJWSTK.DAL
{
    public class GrupaStudenckaDbContext : IGrupaStudenckaDbService
    {
        public IEnumerable<GrupaStudencka> GetGrupaStudenckaForCurrentSemester()
        {
            List<GrupaStudencka> grupy = null; 

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_GrupaStudencka_For_Current_Semestr", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        grupy = new List<GrupaStudencka>();
                        GrupaStudencka gr = null;
                        
                        while (dr.Read())
                        {
                            gr = new GrupaStudencka();
                            gr.IdGrupy = (int)dr["IdGrupaStudencka"];
                            gr.NrGrupy = dr["Nr_grupy"].ToString();
                            gr.IdStudia = (int)dr["IdStudia"];
                            gr.ITN = dr["TylkoITN"].ToString();
                            gr.NrSemestru = (int)dr["NrSemestru"];
                            grupy.Add(gr);
                        }
                    }
                    return grupy;
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine("ExcGrupy : " + grupy.Count + exc);
                return grupy;
            }
        }

        public bool AddGrupaStudenckaToAnkieta(int idGrupaStud, Ankieta ankieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_GrupaStudencka_To_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idGrupaStud", SqlDbType.Int).Value = idGrupaStud;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.Int).Value = ankieta.IdAnkieta;

                    con.Open();
                    cmd.ExecuteReader();
                    return true;

                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IEnumerable<GrupaStudencka> GetGrupaStudenckaByAnkieta(int idAnkieta)
        {
            List<GrupaStudencka> grupy = null;

            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_GrupaStudencka_By_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        grupy = new List<GrupaStudencka>();
                        GrupaStudencka gr = null;

                        while (dr.Read())
                        {
                            gr = new GrupaStudencka();
                            gr.IdGrupy = (int)dr["IdGrupaStudencka"];
                            gr.NrGrupy = dr["Nr_grupy"].ToString();
                            gr.IdStudia = -1;
                            gr.ITN = dr["ITN"].ToString();
                            gr.NrSemestru = -1;
                            grupy.Add(gr);
                        }
                    }
                   
                    return grupy;
                }
            }
            catch (Exception exc)
            {
                return grupy;
            }
        }

    }
}