﻿using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SystemAnkietPJWSTK.DAL
{
    public class RespondentDbContext : IRespondentDbService
    {
        public bool AddOsobaToAnkieta(int idOsoba, Ankieta ankieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_Osoba_To_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("idOsoba", SqlDbType.Int).Value = idOsoba;
                    cmd.Parameters.Add("idAnkieta", SqlDbType.Int).Value = ankieta.IdAnkieta;

                    con.Open();
                    cmd.ExecuteReader();
                    return true;
                  
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Ankieta_error " + e.ToString());
                return false;
            }
        }

        public bool AddGoscToAnkieta(String email, Ankieta ankieta, String link)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Add_Gosc_To_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Email", SqlDbType.VarChar).Value = email;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = ankieta.IdAnkieta;
                    cmd.Parameters.Add("Link", SqlDbType.VarChar).Value = link;

                    con.Open();
                    cmd.ExecuteReader();
                    return true;

                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Osoba> GetAllRespondents()
        {
            List<Osoba> respondents = null;
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_All_Respondents", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        respondents = new List<Osoba>();
                        Osoba o = null;

                        while (dr.Read())
                        {
                            o = new Osoba();
                            o.IdOsoba = (int)dr["IdOsoba"];
                            o.KontoAD = dr["Konto_AD"].ToString();
                            o.Imie = dr["Imie1"].ToString();
                            o.Nazwisko = dr["Nazwisko"].ToString();
                            o.Email = dr["E_Mail1"].ToString();
                            o.Status = dr["Status"].ToString();

                            respondents.Add(o);
                        }
                    }

                    return respondents;
                }
            }
            catch (Exception e)
            {
                return respondents;
            }
        }

        public IEnumerable<Osoba> GetRespondentByAnkieta(int idAnkieta)
        {
            List<Osoba> respondents = null;
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Respondent_By_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        respondents = new List<Osoba>();
                        Osoba o = null;

                        while (dr.Read())
                        {
                            o = new Osoba();
                            o.IdOsoba = (int)dr["IdOsoba"];
                            o.Imie = dr["Imie1"].ToString();
                            o.Nazwisko = dr["Nazwisko"].ToString();
                            o.Email = dr["E_Mail1"].ToString();

                            respondents.Add(o);
                        }
                    }

                    return respondents;
                }
            }
            catch (Exception e)
            {
                return respondents;
            }
        }

        public int DeleteAllRespondentsByAnkieta(int idAnkieta)
        {
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Delete_All_Respondents_By_Ankieta", con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();
                    int ret = cmd.ExecuteNonQuery();

                    return ret;
                }
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public IEnumerable<RespondentDoRaportu> GetRespondentForRaportByAnkieta(int idAnkieta)
        {
            List<RespondentDoRaportu> respondenci = null;
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Respondent_For_Report_By_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        respondenci = new List<RespondentDoRaportu>();
                        RespondentDoRaportu respondent;
                        while (dr.Read())
                        {
                            respondent = new RespondentDoRaportu(
                                dr["KontoAD"].ToString(),
                                (int)dr["IdRespondent"],
                                dr["Imie"].ToString(),
                                dr["Nazwisko"].ToString(),
                                dr["Email"].ToString(),
                                (bool)dr["CzyWyp"],
                                (bool)dr["CzyAnonim"]   
                            );
                            respondenci.Add(respondent);
                        }
                    }

                    return respondenci;
                }
            }
            catch (Exception exc)
            {
                Utils.Log.d(exc.ToString());
                return respondenci;
            }
        }

        public IEnumerable<RespondentDoRaportu> GetRespondentWithWypelnienieByAnkieta(int idAnkieta) 
        {
            List<RespondentDoRaportu> respondenci = null;
            try
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["SystemAnkietDb"].ConnectionString))
                using (var cmd = new SqlCommand("ANK_Get_Respondent_With_Wypelnienie_By_Ankieta", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("IdAnkieta", SqlDbType.Int).Value = idAnkieta;
                    con.Open();

                    using (var dr = cmd.ExecuteReader())
                    {
                        respondenci = new List<RespondentDoRaportu>();
                        RespondentDoRaportu respondent;
                        while (dr.Read())
                        {
                            respondent = new RespondentDoRaportu(
                                dr["KontoAD"].ToString(),
                                (int)dr["IdRespondent"],
                                dr["Imie"].ToString(),
                                dr["Nazwisko"].ToString(),
                                dr["Email"].ToString(),
                                true,
                                (bool)dr["CzyAnonim"]
                            );
                            respondenci.Add(respondent);
                        }
                    }

                    return respondenci;
                }
            }
            catch (Exception exc)
            {
                Utils.Log.d(exc.ToString());
                return respondenci;
            }
        }
    }
}