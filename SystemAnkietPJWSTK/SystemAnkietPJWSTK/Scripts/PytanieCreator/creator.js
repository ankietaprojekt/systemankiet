﻿//const
var MAX_ANS_NUM = 10;

//sortable

$(function () {
    $(".sortable").sortable({
        handle: ".sortable-handle",
        axis: "y",
        containment: '#qstForm',
    });
   // $(".sortable").disableSelection();
});

//qst = question

var counter = 0; //it is incremented after add each question. Removing question does not decrement it!
var questionTypes = { //enum with question types
    simpleText: 1,
    fullText: 2,
    singleAns: 3,
    multiAns: 4
}

var qstList = $("#qstList"); //Container with all questions. 
var questionType = null; // question type. Must be deined on start.


function generateToken() {
    var timestamp = new Date().getTime();
    return timestamp + "" + Math.random().toString(36).substring(7);
}

// ----- ajax 

function saveQuestion(qstOrder, type, title, props, token) {
   
    $.ajax({
        url: "Pytanie/NowePytanie",
        type: "POST",
        async: false,
        data: { "title": escape(title), "type": type, "props": JSON.stringify(props), "token": token },
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            // window.location.href = "/Ankieta/PresubmitPreview";
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

//----

// ----- questions functions

// this function generate questions from session
function addQst(qstTitle, qstType, answers) {
    console.log("Read qst from session: " + qstTitle + ", " + qstType + ", " + answers);
    var qstSkeleton = prepareQstSkeleton(qstType);
    qstSkeleton.find(".qst-title").val(qstTitle);
    var qstBox = qstSkeleton.find('.qst-box');
    var qstAnswers = $("<div />", {
        class: "qst-answers",
        style: "clear:both"
    });
    qstBox.append(qstAnswers);


    var addAnswerLink = $("<button />", {
        class: "k-button addAnswerButton",
        click: function () {
            addAnswer(qstType, $(this).parent());
        },
        text: "Kolejna odpowiedź"
    });

    var qstType = parseInt(qstType);
    switch (qstType) {
        case questionTypes.simpleText:
            qstAnswers.append(addTextQst());
            break;

        case questionTypes.fullText:
            qstAnswers.append(addFullTextQst());
            break;

        case questionTypes.singleAns:
            answers = answers.replace(/&quot;/g, "\"");
            var jsonAnswers = jQuery.parseJSON(answers);
            $.each(jsonAnswers, function (i, val) {
                qstAnswers.append(getRadioAnswer(val));
            });
            qstBox.append(addAnswerLink);
            break;

        case questionTypes.multiAns:
            answers = answers.replace(/&quot;/g, "\"");
            var jsonAnswers = jQuery.parseJSON(answers);
            $.each(jsonAnswers, function (i, val) {
                qstAnswers.append(getChbAnswer(val));
            });
            qstBox.append(addAnswerLink);
            break;
    }

    qstList.append(qstSkeleton);
    setKendoIcons();
}

//this function create question skeleton and based on qustion type build appropriate question body.   
function addNewQst(qstType) {
    var qstSkeleton = prepareQstSkeleton(qstType);
    var qstBox = qstSkeleton.find('.qst-box');
    var qstAnswers = $("<div />", {
        class: "qst-answers",
        style: "clear:both"
    });
    qstBox.append(qstAnswers);


    var addAnswerLink = $("<button />", {
        class: "k-button addAnswerButton",
        click: function () {
            addAnswer(qstType, $(this).parent());
        },
        text: "Kolejna odpowiedź"
    });

    switch (qstType) {
        case questionTypes.simpleText:
            qstAnswers.append(addTextQst());
            break;

        case questionTypes.fullText:
            qstAnswers.append(addFullTextQst());
            break;

        case questionTypes.singleAns:
            qstAnswers.append(getRadioAnswer(""));
            qstAnswers.append(getRadioAnswer(""));
            qstAnswers.append(getRadioAnswer(""));
            qstBox.append(addAnswerLink);
            break;

        case questionTypes.multiAns:
            qstAnswers.append(getChbAnswer(""));
            qstAnswers.append(getChbAnswer(""));
            qstAnswers.append(getChbAnswer(""));
            qstBox.append(addAnswerLink);
            break;

        default:
            console.log("wrong option");
            return;
            break;
    }

    qstList.append(qstSkeleton);
    setKendoIcons();
    applyKendoEditor();

}

function prepareQstSkeleton(qstType) {
    var qstSkeleton = $("<li />", {
        style: "margin:10px 0 10px 0",
        class: "qst-row-li"
    });
    var qstHandle = $("<span />", {
        class: "sortable-handle",
        style: "cursor: pointer;visibility:hidden"
    });
    var qstDiv = $("<div />", {
        id: "qst-" + (++counter),
        class: "qst-box qst-type-" + qstType
    });
    var qstActionDiv = $("<div />", {
        class: "qst-lbl",
        style: "float:left"
    });

    var txtarea = $("<textarea />", {
        class: "k-textbox qst-title enabled",
        placeholder: "Treść pytania",
        style: "width:555px; height:100px; margin-bottom:20px;,",
        rows: "5"
    });

    var qstLbl = $("<label />").append(
           txtarea
        );
    var remLink = $("<a />", {
        class: "removeQst"
    });
    var confLink = $("<a />", {
        class: "saveQst"
    });

    var iconsDiv = $("<div />", {
        class: "icons-div",
        style: "float:right"
    });

    iconsDiv.append(remLink);
    iconsDiv.append(confLink);

    qstSkeleton.append(qstDiv);
    qstDiv.append(qstActionDiv);
    qstActionDiv.append(qstHandle);
    qstActionDiv.append(qstLbl);
    qstDiv.append(iconsDiv);

    return qstSkeleton;
}

//simple text answer question
function addTextQst() {
    return $("<input />", {
        class: "disabled k-textbox one-line-response",
        type: "text",
        disabled: "disabled"
    });
}

//full text anwser question
function addFullTextQst() {
    return $("<textarea />", {
        class: "disabled k-textarea fulltext-response",
        cols: "34",
        rows: "3",
        disabled: "disabled"
    });
}


function changeQstType(qstType) {
    questionType = questionTypes[qstType];
}


function removeQst(qstBox) {
    qstBox.remove();
}

// -- answers functions 

function getAnswerSpan() {
    return $("<span />", {
        class: 'answer'
    });
}

function getRadioAnswer(text) {
    var span = getAnswerSpan();
    var inputRadio = $("<input />", {
        class: 'disabled',
        type: 'radio',
        disabled: 'disabled'
    });
    var inputText = $("<input />", {
        class: 'prop k-textbox',
        type: 'text',
        value: text
    });
    var remLink = $("<a />", {
        class: 'removeAnswer',
        click: function () {
            removeAnswer($(this).parent());
        }
    });

    span.append(inputRadio);
    span.append(inputText);
    span.append(remLink);

    return span;
}

function getChbAnswer(text) {
    var span = getAnswerSpan();
    var inputChb = $("<input />", {
        class: 'disabled',
        type: 'checkbox',
        disabled: 'disabled'
    });
    var inputText = $("<input />", {
        class: 'prop k-textbox',
        type: 'text',
        value: text
    });
    var remLink = $("<a />", {
        class: 'removeAnswer',
        click: function () {
            removeAnswer($(this).parent());
        }
    });

    span.append(inputChb);
    span.append(inputText);
    span.append(remLink);

    return span;
}

function addAnswer(qstType, box) { //add new answer for questions. Max. number is defined as global MAX_ANS_NUM
    var answerBox = box.find(".qst-answers");
    var countAnswer = box.find("span.answer").length;
    if (countAnswer == MAX_ANS_NUM) return;

    switch (qstType) {
        case questionTypes.singleAns:
            answerBox.append(getRadioAnswer(""));
            break;

        case questionTypes.multiAns:
            box.find(".qst-answers").append(getChbAnswer(""));
            break;
    }

    setKendoIcons();

    if (countAnswer >= 3) {
        enableRemoveButtons(answerBox);
    }
}

function removeAnswer(answer) { //remove answers from question. Until leave not less than 2.
    var countAnswer = answer.parent().find("span.answer").length;
    var answerBox = answer.parent();
    if (countAnswer > 2) {
        answer.remove();
        if (countAnswer == 3) {
            disableRemoveButtons(answerBox);
        }
    }
}

function enableRemoveButtons(answerBox) { //show 'remove button' for checkbox and radio buttons
    answerBox.find(".removeAnswer").each(function () {
        $(this).css("display", "inline-block");
    });
}

function disableRemoveButtons(answerBox) { //hide 'remove button' for checkbox and radio buttons
    answerBox.find(".removeAnswer").each(function () {
        $(this).css("display", "none");
    });
}

function confirmQst(qstRow) {
    // answers
    var answers = qstRow.find('.answer');
    var ansValid = true;
    answers.find('input[type="text"]').each(function () {
        if ($(this).val() !== '') {
            $(this).addClass('confirmed').attr('disabled', 'disabled');
            $(this).removeClass('not-valid');
        } else {
            $(this).addClass('not-valid');
            ansValid = false;
        }
    });

    // question title
    var qstInp = qstRow.find('.qst-title');
    var editorTable = qstRow.find('table.k-editor');
    var editor = qstInp.data("kendoEditor");

    if (editor.value() !== "") {
        editorTable.removeClass('not-valid');
    } else {
        editorTable.addClass('not-valid');
    }
    if (editor.value() !== "" && ansValid) {
        var editor = qstInp.data("kendoEditor");
        var editorContent = editor.value();
        qstInp.remove();
        qstRow.find(".qst-lbl").find("label").html(editorContent);

        qstRow.find(".removeQst").hide();
        var saveQstBtn = qstRow.find(".saveQst");
        saveQstBtn.addClass('confirmed');
        saveQstBtn.removeClass("saveQst")
        saveQstBtn.find("span").removeClass('k-i-tick');
        saveQstBtn.kendoButton({
            icon: "pencil"
        });
        disableRemoveButtons(answers);
        qstRow.find(".addAnswerButton").hide();
        qstRow.find(".sortable-handle").css("visibility", "visible");
    } else {
        answers.find('input[type="text"]').each(function () {
            $(this).removeClass('confirmed').removeAttr('disabled');
        });
    }
}

function editQst(qstRow) {
    var qstLbl = qstRow.find('.qst-lbl').find("label");
    var content = qstLbl.html();
    qstLbl.html("");
    var txtarea = $("<textarea />", {
        class: "k-textbox qst-title enabled editor-on",
        placeholder: "Treść pytania",
        style: "width:555px; height:100px; margin-bottom:20px;",
    });
    txtarea.html(content);
    qstLbl.append(txtarea);
    txtarea.kendoEditor();

    //  qstInp.removeClass('confirmed').removeAttr('disabled');

    qstRow.find(".removeQst").show();
    var saveQstBtn = qstRow.find(".k-button-icon.confirmed");
    saveQstBtn.removeClass('confirmed');
    saveQstBtn.addClass("saveQst");
    saveQstBtn.find("span").removeClass('k-i-pencil');
    saveQstBtn.kendoButton({
        icon: "tick"
    });

    var answers = qstRow.find('.answer');
    answers.find('input[type="text"]').each(function () {
        $(this).removeClass('confirmed').removeAttr('disabled');
    });

    enableRemoveButtons(answers);
    qstRow.find(".addAnswerButton").show();
    qstRow.find(".sortable-handle").css("visibility", "hidden");
}
function setKendoIcons() {
    // set kendo icons
    $(".saveQst").each(function () {

        $(this).kendoButton({
            icon: "tick"
        });
    });

    $(".removeQst").kendoButton({
        icon: "close"
    });

    $(".removeAnswer").kendoButton({
        icon: "close"
    });
}

function saveAllQuestions() {
    var qstOrder = 0;
    var propsOrder = 0;
    var type = 0;
    var answer_div;
    var qstBox;
    var token = generateToken();
    qstList.find("li.qst-row-li").each(function () {
        qstOrder++;
        var qstBox = $(this).find("div.qst-box");
        if (qstBox.hasClass("qst-type-0"))
            type = 0;
        else if (qstBox.hasClass("qst-type-1"))
            type = 1;
        else if (qstBox.hasClass("qst-type-2"))
            type = 2;
        else if (qstBox.hasClass("qst-type-3"))
            type = 3;
        else if (qstBox.hasClass("qst-type-4"))
            type = 4;
        var title = qstBox.find(".qst-lbl").find("label").html();
        console.log(title);
        var props = null;
        if (type == 3 || type == 4) {
            props = {};
            answer_div = qstBox.find("span.answer");
            propsOrder = 0;
            answer_div.find("input.prop").each(function () {
                props[propsOrder++] = $(this).val();
            });
        }
       saveQuestion(qstOrder, type, title, props, token);
    });
}

function isQstListEmpty(qstList) {
    var questionCounter = 0;
    qstList.find("li.qst-row-li").each(function () {
        questionCounter++;
    });

    if (questionCounter == 0) {
        alert('Ankieta musi zawierać przynajmniej jedno pytanie!');
        return false;
    }
}

function applyKendoEditor() {
    qstList.find(".qst-title.enabled").each(function () {
        if (!$(this).hasClass('editor-on'))
            $(this).addClass('editor-on').kendoEditor();

    });
}

function validateQuestions(qstList) {
    var counter = 0;
    qstList.find(".qst-title.enabled").each(function () {
        counter++;
    });
    if(counter > 0)
        return false;
    else
        return true;
}


// ----  main
$(document).ready(function () {
    // create kendo drop down list with question type
    $("#question-type-list").width(250).kendoDropDownList();

    // box for questions
    qstList = $("#qstList");
    qstTypesList = $("#question-type-list");
    questionType = questionTypes[qstTypesList.val()]; //set default question type from html list

    //if there are already set any question, increment ID
    var incID = null;
    $(".qst-box").each(function () {
        incID = $(this).attr('id').split("-")[1];
        if (incID > counter)
            counter = incID;
    });

    //new question
    $("#addNewQstBtn").on("click", function () {
        addNewQst(questionType);
    });

    //removing question
    qstList.on("click", ".removeQst", function () {
        var qstBox = $(this).parent().parent().parent();
        removeQst(qstBox);
    });

    $(qstList).on("click", ".saveQst", function () {
        var p = $(this).parent().parent();
        confirmQst(p);
    });

    $(qstList).on("click", ".confirmed", function () {
        var p = $(this).parent().parent();
        editQst(p);
    });

    $("#submitButton").on("click", function () {
        if (isQstListEmpty(qstList) == false)
            return false;
        if (validateQuestions(qstList) == true) {
            saveAllQuestions();
            var url = BASE_URL + '/Respondenci';
          window.location.href = url;
        } else {
            alert("Musisz zatwierdzić wszystkie pytania!");
        }
    });

});
