﻿kendo.cultures["pl-PL"] = {
    //<language code>-<country/region code>
    name: "pl-PL",
    // "numberFormat" defines general number formatting rules
    numberFormat: {
        //numberFormat has only negative pattern unline the percent and currency
        //negative pattern: one of (n)|-n|- n|n-|n -
        pattern: ["-n"],
        //number of decimal places
        decimals: 2,
        //string that separates the number groups (1,000,000)
        ",": ",",
        //string that separates a number from the fractional point
        ".": ".",
        //the length of each number group
        groupSize: [3],
        //formatting rules for percent number
        percent: {
            //[negative pattern, positive pattern]
            //negativePattern: one of -n %|-n%|-%n|%-n|%n-|n-%|n%-|-% n|n %-|% n-|% -n|n- %
            //positivePattern: one of n %|n%|%n|% n
            pattern: ["-n %", "n %"],
            //number of decimal places
            decimals: 2,
            //string that separates the number groups (1,000,000 %)
            ",": ",",
            //string that separates a number from the fractional point
            ".": ".",
            //the length of each number group
            groupSize: [3],
            //percent symbol
            symbol: "%"
        },
        currency: {
            //[negative pattern, positive pattern]
            //negativePattern: one of "($n)|-$n|$-n|$n-|(n$)|-n$|n-$|n$-|-n $|-$ n|n $-|$ n-|$ -n|n- $|($ n)|(n $)"
            //positivePattern: one of "$n|n$|$ n|n $"
            pattern: ["($n)", "$n"],
            //number of decimal places
            decimals: 2,
            //string that separates the number groups (1,000,000 $)
            ",": ",",
            //string that separates a number from the fractional point
            ".": ".",
            //the length of each number group
            groupSize: [3],
            //currency symbol
            symbol: "$"
        }
    },
    calendars: {
        standard: {
            days: {
                // full day names
                names: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
                // abbreviated day names
                namesAbbr: ["Nd", "Pon", "Wt", "Śr", "Czw", "Pt", "Sob"],
                // shortest day names
                namesShort: [ "Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "Sb" ]
            },
            months: {
                // full month names
                names: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesiń", "Październik", "Listopad", "Grudzień"],
                // abbreviated month names
                namesAbbr: ["Sty", "Lut", "Mar", "kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"]
            },
            // AM and PM designators
            // [standard,lowercase,uppercase]
            AM: [ "AM", "am", "AM" ],
            PM: [ "PM", "pm", "PM" ],
            // set of predefined date and time patterns used by the culture.
            patterns: {
                d: "M/d/yyyy",
                D: "dddd, dd MMMM, yyyy",
                F: "dddd, MMMM dd, yyyy h:mm:ss tt",
                g: "M/d/yyyy h:mm tt",
                G: "M/d/yyyy h:mm:ss tt",
                m: "MMMM dd",
                M: "MMMM dd",
                s: "yyyy'-'MM'-'ddTHH':'mm':'ss",
                t: "HH:mm",
                T: "h:mm:ss tt",
                u: "yyyy'-'MM'-'dd HH':'mm':'ss'Z'",
                y: "MMMM, yyyy",
                Y: "MMMM, yyyy"
            },
            // the first day of the week (0 = Sunday, 1 = Monday, etc)
            firstDay: 1
        }
    }
};