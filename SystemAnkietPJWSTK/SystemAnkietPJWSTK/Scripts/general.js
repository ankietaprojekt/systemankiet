﻿function show_top_alert(positivie, text) {
    var alert_box = $("<div />", {
        class: "alert-box"
    });

    var alert_span = $("<div />", {
        class: "alert-span"
    });

    alert_box.append(alert_span);

    if (positivie)
        alert_span.addClass('positive');
    else
        alert_span.addClass('negative');

    alert_span.append("<span>" + text + "</span>");
    $("body").append(alert_box);

    alert_box.fadeIn(1500).delay(2500).fadeOut(1500);
}