﻿//const
var MAX_ANS_NUM = 10;

//sortable

$(function () {
    $(".sortable").sortable({
        handle: ".sortable-handle"
    });
    $(".sortable").disableSelection();
});

//qst = question

var counter = 0; //it is incremented after add each question. Removing question does not decrement it!
var questionTypes = { //enum with question types
    simpleText: 1,
    fullText: 2,
    singleAns: 3,
    multiAns: 4
}

var qstList = null; //Container with all questions. It must be defined in document.ready function !
var questionType = null; // question type. Must be deined on start.

// ----- questions functions

//this function create question skeleton and based on qustion type build appropriate question body.
function addNewQst(qstType) {
    var qstSkeleton = "<li><span class=\"sortable-handle\" style=\"cursor:pointer\">H</span>" +
					  "<div id=\"qst-" + (++counter) + "\" class=\"qst-box\">" +
					  "<div class=\"qst-lbl\"><label><input type=\"text\" placeholder=\"Treść pytania\"></label>" +
					  "<a class=\"removeQst\">Usun to pytanie</a></div>" +
					  "<div class=\"qst-answers\"></div>";

    var qstBody = null;
    switch (qstType) {
        case questionTypes.simpleText:
            qstBody = addTextQst();
            break;

        case questionTypes.fullText:
            qstBody = addFullTextQst();
            break;

        case questionTypes.singleAns:
            qstBody = addSingleRespQst();
            qstSkeleton += "<a onclick=\"addAnswer(" + qstType + ", $(this).parent())\">Dodaj odpowiedź</a>";
            break;

        case questionTypes.multiAns:
            qstBody = addMultiRespQst();
            qstSkeleton += "<a onclick=\"addAnswer(" + qstType + ", $(this).parent())\">Dodaj odpowiedź</a>";
            break;

        default:
            console.log("wrong option");
            return;
            break;
    }

    qstSkeleton += "</div></li>";

    qstList.append(qstSkeleton);
    $("#qst-" + (counter)).find(".qst-answers").append(qstBody);
}

//simple text answer question
function addTextQst() {
    return "<input class=\"disabled\" type=\"text\"/>";
}

//full text anwser question
function addFullTextQst() {
    return "<textarea class=\"disabled\"></textarea>";
}

//only one answer to choose
function addSingleRespQst() {
    var answ = getRadioAnswer("Choose me") + getRadioAnswer("Choose me") + getRadioAnswer("Choose me");
    return answ;
}

//multiply answer
function addMultiRespQst() {
    var answ = getChbAnswer("Choose me") + getChbAnswer("Choose me") + getChbAnswer("Choose me");
    return answ;
}


function changeQstType(qstType) {
    questionType = questionTypes[qstType];
}


function removeQst(qstBox) {
    qstBox.remove();
}

// -- answers functions 

function getRadioAnswer(text) {
    return "<span class=\"answer\"><input class=\"disabled\" type=\"radio\"/><input type=\"text\" value=\"" + text + "\"/><a class=\"removeAnswer\" onclick=\"removeAnswer($(this).parent())\">usun</a></span>";
}

function getChbAnswer(text) {
    return "<span class=\"answer\"><input class=\"disabled\" type=\"checkbox\"/><input type=\"text\" value=\"" + text + "\"/><a class=\"removeAnswer\" onclick=\"removeAnswer($(this).parent())\">usun</a></span>";
}

function addAnswer(qstType, box) { //add new aserws for questions. Max. number is defined as global MAX_ANS_NUM
    var answerBox = box.find(".qst-answers");
    var countAnswer = box.find("span.answer").length;
    if (countAnswer == MAX_ANS_NUM) return;

    switch (qstType) {
        case questionTypes.singleAns:
            box.find(".qst-answers").append(getRadioAnswer("nowa"));
            break;

        case questionTypes.multiAns:
            box.find(".qst-answers").append(getChbAnswer("nowa"));
            break;
    }

    if (countAnswer >= 3) {
        enableRemoveButtons(answerBox);
    }
}

function removeAnswer(answer) { //remove answers from question. Until leave not less than 2.
    var countAnswer = answer.parent().find("span.answer").length;
    var answerBox = answer.parent();
    if (countAnswer > 2) {
        answer.remove();
        if (countAnswer == 3) {
            disableRemoveButtons(answerBox);
        }
    }
}

function enableRemoveButtons(answerBox) { //show 'remove button' for checkbox and radio buttons
    answerBox.find("span a.removeAnswer").each(function () {
        $(this).css("display", "inline-block");
    });
}

function disableRemoveButtons(answerBox) { //hide 'remove button' for checkbox and radio buttons
    answerBox.find("span a.removeAnswer").each(function () {
        $(this).css("display", "none");
    });
}


// ----  main
$(document).ready(function () {

    qstList = $("#qstList");
    qstTypesList = $("#question-type-list");
    questionType = questionTypes[qstTypesList.val()]; //set default question type from html list



    //new question
    $("#addNewQstBtn").on("click", function () {
        addNewQst(questionType);
    });

    //removing question
    qstList.on("click", ".removeQst", function () {
        var qstBox = $(this).parent().parent();
        removeQst(qstBox);
    });

});
