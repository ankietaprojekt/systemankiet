﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{
    [Serializable]
    public class PropOdpowiedz
    {
        [Required]
        public int IdPole { get; set; }

        [StringLength(500, MinimumLength = 1)]
        public string PropTresc { get; set; }

        public int IdPytanie { get; set; }
    }
}