﻿namespace SystemAnkietPJWSTK.Models
{
    public class ViewModel
    {
        public double Percentage { get; set; }
        public string Value { get; set; }
        public string Color { get; set; }
    }
}