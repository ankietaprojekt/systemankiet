﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{
    [Serializable]
    public class Odpowiedz
    {
        [Required]
        public int IdOdpowiedz { get; set; }

        public int IdWypelnienie { get; set; }

        public int IdPole { get; set; }

        public int IdPytanie { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime OdpCzasowa { get; set; }

        public string TrescOdpOtwartej { get; set; }

    }
}