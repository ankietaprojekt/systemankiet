﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SystemAnkietPJWSTK.Models
{
    public class CzesciowaOdpowiedz
    {
        public int Id { get; set; }
        public String Tresc { get; set; }
        public String Typ { get; set; }
    }
}