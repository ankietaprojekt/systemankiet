﻿namespace SystemAnkietPJWSTK.Models
{
    public class Gosc
    {
        public int IdGosc { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}