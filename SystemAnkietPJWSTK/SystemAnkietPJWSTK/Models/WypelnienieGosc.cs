﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{
    public class WypelnienieGosc : Wypelnienie
    {
        [Required]
        [Display(Name = "Link")]
        public String Link { get; set; }
    }
}