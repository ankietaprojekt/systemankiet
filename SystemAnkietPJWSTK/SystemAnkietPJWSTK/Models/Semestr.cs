﻿using System;

namespace SystemAnkietPJWSTK.Models
{
    public class Semestr
    {
        public String NrSemestru { get; set; }
        public int IdStudia { get; set; }
    }
}