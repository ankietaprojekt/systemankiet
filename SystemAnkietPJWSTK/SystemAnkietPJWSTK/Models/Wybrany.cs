﻿namespace SystemAnkietPJWSTK.Models
{
    public class Wybrany
    {
        public int ID { get; set; }
        public string Nazwa { get; set; }
        public string Typ { get; set; }
        public int ParentID { get; set; }
    }
}