﻿using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{
    public class RespondentDoRaportu
    {
        public string KontoAD { get; set; }
        public int ID { get; set; }
        [Display(Name = "Imię")]
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public bool czyWypelnil { get; set; }
        public bool czyAnonim { get; set; }

        public RespondentDoRaportu(string ad, int id, string imie, string nazwisko, string email, bool wypelnil, bool anonim)
        {
            this.KontoAD = ad;
            this.ID = id;
            this.Imie = imie;
            this.Nazwisko = nazwisko;
            this.Email = email;
            this.czyWypelnil = wypelnil;
            this.czyAnonim = anonim;
        }
    }
}