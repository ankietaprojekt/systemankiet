﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{   

    [Serializable]
    public class Ankieta
    {
        [Required]
        public int IdAnkieta { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data utworzenia")]
        public DateTime DataUtw { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.lang_pl), ErrorMessageResourceName = "date_start_req_error")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data rozpoczęcia")]
        public DateTime DataRozp { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.lang_pl), ErrorMessageResourceName = "date_end_req_error")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data zamknięcia")]
        public DateTime DataZam { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.lang_pl), ErrorMessageResourceName = "date_rem_req_error")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data modyfikacji")]
        public DateTime DataMod { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.lang_pl), ErrorMessageResourceName = "ankieta_name_req_error")]
        [StringLength(100, MinimumLength = 1)]
        [Display(Name = "Nazwa")]
        public string Nazwa { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Opis")]
        public string Opis { get; set; }

        [Display(Name = "Treść maila")]
        [DataType(DataType.MultilineText)]
        public string TrescMaila { get; set; }

        [Required]
        [Display(Name = "Możliwość edycji przez respondenta")]
        public bool CzyEdyt { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data przypomnienia")]
        public DateTime? CzasPrzyp { get; set; }

        [Required]
        [Display(Name = "Ankieta anonimowa")]
        public bool CzyAnonim { get; set; }

        [Display(Name = "Autor")]
        public virtual int IdOsoba { get; set; }

        [Display(Name = "Respondent")]
        public virtual Osoba Respondent { get; set; }

        [Display(Name = "Ilość odpowiedzi")]
        public int IloscOdpowiedzi { get; set; }

        [Display(Name = "Ilość respondentów")]
        public int IloscRespondentow { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Ustaw przypomnienie")]
        public bool CzyPrzypomnienie { get; set; }

        public Ankieta()
        {
            DataUtw = DateTime.Now;

            DataRozp = DateTime.Now;
            DataRozp = DataRozp.AddMinutes(60 - DataRozp.Minute);

            DataZam = DateTime.Now.AddDays(1);
            DataZam = DataZam.AddMinutes(60 - DataZam.Minute);

            DateTime tmpCzasPrzyp =  DataZam.AddHours(-12);
            tmpCzasPrzyp.AddMinutes(60 - tmpCzasPrzyp.Minute);
            CzasPrzyp = tmpCzasPrzyp;

            CzyPrzypomnienie = false;
        }
    }
}