﻿using System;

namespace SystemAnkietPJWSTK.Models
{
    public class GrupaStudencka
    {
        public int IdGrupy { get; set; }
        public String NrGrupy { get; set; }
        public int IdStudia { get; set; }
        public String ITN { get; set; }
        public int NrSemestru { get; set; }
    }
}