﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{
    public class Wypelnienie
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public int IdAnkieta { get; set; }
        public DateTime DataMod { get; set; }

        public virtual Ankieta ankieta { get; set; }

    }
}