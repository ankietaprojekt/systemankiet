﻿using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{
    public class WypelnienieUser : Wypelnienie
    {
        [Required]
        public int IdUser { get; set; }

        public virtual Osoba osoba { get; set; }
    }
}