﻿namespace SystemAnkietPJWSTK.Models
{
    public class Login
    {
        public int userID { get; set; }
        public string userPass { get; set; }
        public string userName { get; set; }
        public bool tworca { get; set; }
    }
}