﻿using System.ComponentModel.DataAnnotations;

namespace SystemAnkietPJWSTK.Models
{
    public class Osoba
    {
        [Required]
        [Display(Name = "Respondent")]
        public int IdOsoba { get; set; }
        public string KontoAD { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
    }
}