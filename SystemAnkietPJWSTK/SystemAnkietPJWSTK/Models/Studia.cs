﻿namespace SystemAnkietPJWSTK.Models
{
    public class Studia
    {
        public int IdStudia { get; set; }
        public string Nazwa { get; set; }
        public int IdWydzial { get; set; }
    }
}