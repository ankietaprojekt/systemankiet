﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;



namespace SystemAnkietPJWSTK.Models
{
    public class Pytanie
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int IdAnkieta { get; set; }

        [Required]
        [Display(Name = "Typ")]
        public int Typ { get; set; }

        [Required]
        [Display(Name = "Treść")]
        public String Tresc { get; set; }
        
        [Display(Name = "Minimalna ilość odpowiedzi")]
        public int MinOdpowiedzi { get; set; }
        
        [Display(Name = "Maksymalna ilość odpowiedzi")]
        public int MaxOdpowiedzi { get; set; }

        public virtual Ankieta ankieta { get; set; }

        public virtual List<PropOdpowiedz> props { get; set; }

        public virtual List<Odpowiedz> odpowiedzi { get; set; }
        
    }
}