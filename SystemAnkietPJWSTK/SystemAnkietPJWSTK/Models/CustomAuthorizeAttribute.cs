﻿using System.Web;
using System.Web.Mvc;

namespace SystemAnkietPJWSTK.Models
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public CustomAuthorizeAttribute() { }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if (
                System.Web.HttpContext.Current.Session["logged"] != null &&
                (bool)System.Web.HttpContext.Current.Session["logged"] == true &&
                System.Web.HttpContext.Current.Session["loggedUser"] != null)
            {
                return true;
            }

            return false;
        }
    }
}
