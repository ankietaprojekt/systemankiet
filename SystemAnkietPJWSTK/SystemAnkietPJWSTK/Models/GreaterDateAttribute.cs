﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SystemAnkietPJWSTK.Models
{   
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class GreaterDateAttribute : ValidationAttribute, IClientValidatable
    {
        public string EarlierDateField { get; set; }
        public string GreaterDateField { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime? date = value != null ? (DateTime?)value : null;

            var earlierDateValue = validationContext.ObjectType.GetProperty(EarlierDateField).GetValue(validationContext.ObjectInstance, null);

            DateTime? earlierDate = earlierDateValue != null ? (DateTime?)earlierDateValue : null;
            DateTime? greaterDate = null;
            if (GreaterDateField != null)
            {
               var  greaterDateValue = validationContext.ObjectType.GetProperty(GreaterDateField).GetValue(validationContext.ObjectInstance, null);
               greaterDate = greaterDateValue != null ? (DateTime?)greaterDateValue : null;
            }

            if ((date.HasValue) && ( (earlierDate.HasValue && date <= earlierDate) || (greaterDate != null && greaterDate.HasValue && greaterDate <= date ) ) )
            {
                return new ValidationResult(ErrorMessage);
            }

            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
 	         var rule = new ModelClientValidationRule
                {
                    ErrorMessage = ErrorMessage,
                    ValidationType = "greaterdate"        
                };
        
                rule.ValidationParameters["earlierdate"] = EarlierDateField;
                rule.ValidationParameters["greaterdate"] = GreaterDateField;
        
                yield return rule;
        }
    }
}