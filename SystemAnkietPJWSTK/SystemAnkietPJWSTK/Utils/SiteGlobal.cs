﻿using System.Web.Configuration;

namespace SystemAnkietPJWSTK.Utils
{
    public static class SiteGlobal
    {
        public static string BaseUrl { get; set; }

        static SiteGlobal()
        {
            BaseUrl = WebConfigurationManager.AppSettings["BaseUrl"];
        }
    }
}