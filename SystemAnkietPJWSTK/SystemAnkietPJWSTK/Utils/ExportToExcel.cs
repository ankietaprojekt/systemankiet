﻿using System;
using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml;
using System.IO;
using SystemAnkietPJWSTK.Models;
using SystemAnkietPJWSTK.DAL;
using SystemAnkietPJWSTK.DAL.Interfaces;
using System.Text.RegularExpressions;

namespace SystemAnkietPJWSTK.Utils
{
    public static class ExportToExcel
    {
        private static IRespondentDbService dbResp = new RespondentDbContext();
        private static IPytanieDbService dbPyt = new PytanieDbContext();
        private static IPropOdpowiedzDbService dbProp = new PropOdpowiedzDbContext();
        private static IOdpowiedzDbService dbOdp = new OdpowiedzDbContext();
        private static IAnkietaDbService dbAnk = new AnkietaDbContext();

        public static MemoryStream CreateReport(int idAnkiety)
        {
            List<Pytanie> pytania = (List<Pytanie>)dbPyt.GetPytanieByAnkieta(idAnkiety);
            List<RespondentDoRaportu> respondenci = (List<RespondentDoRaportu>)dbResp.GetRespondentWithWypelnienieByAnkieta(idAnkiety);

            MemoryStream stream = CreateFile(pytania, respondenci, idAnkiety);

            stream.Position = 0;

            return stream;
        }

        private static MemoryStream CreateFile(List<Pytanie> pytania, List<RespondentDoRaportu> respondenci, int idAnkiety)
        {

            MemoryStream stream = new MemoryStream();

            stream.Position = 0;

            using (ExcelPackage package = new ExcelPackage())
            {

                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Raport ankiety");

                int firstRespRow = 3;
                int firstRespCol = 1;

                Ankieta ank = dbAnk.GetAnkietaByID(idAnkiety);

                if (!ank.CzyAnonim)
                {
                    foreach (RespondentDoRaportu resp in respondenci)
                    {
                        worksheet.Cells[firstRespRow, firstRespCol].Value = resp.Imie;
                        worksheet.Cells[firstRespRow, firstRespCol + 1].Value = resp.Nazwisko;

                        firstRespRow++;
                    }
                }
                else
                {
                    int i = 1;
                    foreach (RespondentDoRaportu resp in respondenci)
                    {
                        worksheet.Cells[firstRespRow, firstRespCol + 1].Value = "Anonim" + i;

                        firstRespRow++;
                        i++;
                    }
                }

                int firstPytCol = firstRespCol + 2;
                int firstPytRow = 2;

                foreach (Pytanie pyt in pytania)
                {
                    if (pyt.Typ == 3 || pyt.Typ == 4)
                    {
                        pyt.props = (List<PropOdpowiedz>)dbProp.GetPropOdpByPytanie(pyt.Id);

                        worksheet.Cells[firstPytRow - 1, firstPytCol].Value = removeHtmlTags(pyt.Tresc);

                        int firstPropRow = firstPytRow;
                        int firstPropCol = firstPytCol;
                        foreach (PropOdpowiedz prop in pyt.props)
                        {
                            worksheet.Cells[firstPytRow, firstPropCol].Value = prop.PropTresc;

                            int firstOdpRow = firstPytRow + 1;
                            foreach (RespondentDoRaportu resp in respondenci)
                            {
                                List<Odpowiedz> odpowiedzi;
                                if (resp.KontoAD.Equals("-"))
                                {
                                    odpowiedzi = (List<Odpowiedz>)dbOdp.GetOdpowiedzByGuestAnkieta(resp.ID, pyt.IdAnkieta);
                                }
                                else
                                {
                                    odpowiedzi = (List<Odpowiedz>)dbOdp.GetOdpowiedzByUserAnkieta(resp.ID, pyt.IdAnkieta);
                                }

                                bool odp = odpowiedzi.Exists(o => (o.IdPole == prop.IdPole));

                                if (odp)
                                {
                                    worksheet.Cells[firstOdpRow, firstPytCol].Value = 1;
                                }
                                else
                                {
                                    worksheet.Cells[firstOdpRow, firstPytCol].Value = 0;
                                }

                                firstOdpRow++;
                            }

                            firstPropCol++;
                            firstPytCol++;
                        }
                    }
                    else
                    {
                        ExcelRange rng = worksheet.Cells[firstPytRow - 1, firstPytCol, firstPytRow, firstPytCol];
                        rng.Merge = true;

                        worksheet.Cells[firstPytRow - 1, firstPytCol].Value = pyt.Tresc;

                        int firstOdpRow = firstPytRow + 1;
                        foreach (RespondentDoRaportu resp in respondenci)
                        {
                            List<Odpowiedz> odpowiedzi;
                            if (resp.KontoAD.Equals("-"))
                            {
                                odpowiedzi = (List<Odpowiedz>)dbOdp.GetOdpowiedzByGuestAnkieta(resp.ID, pyt.IdAnkieta);
                            }
                            else
                            {
                                odpowiedzi = (List<Odpowiedz>)dbOdp.GetOdpowiedzByUserAnkieta(resp.ID, pyt.IdAnkieta);
                            }

                            Odpowiedz odp = odpowiedzi.Single(o => o.IdPytanie == pyt.Id);
                            worksheet.Cells[firstOdpRow, firstPytCol].Value = odp.TrescOdpOtwartej;

                            firstOdpRow++;
                        }

                        firstPytCol++;
                    }
                }

                worksheet.Cells.AutoFitColumns(0);

                package.SaveAs(stream);

            }

            stream.Position = 0;

            return stream;
        }

        private static String removeHtmlTags(String htmlString)
        {
            return Regex.Replace(htmlString, @"<[^>]*>", String.Empty);
        }
    }
 
}
