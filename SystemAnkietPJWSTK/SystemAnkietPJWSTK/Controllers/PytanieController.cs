﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SystemAnkietPJWSTK.Models;

namespace SystemAnkietPJWSTK.Controllers
{
    [CustomAuthorizeAttribute]
    public class PytanieController : Controller
    {
        public ActionResult NowePytanie()
        {
            return View();
        }

        [HttpPost]
        public void NowePytanie(string title, string type, string props, string token)
        {
            Pytanie p = new Pytanie();
            p.Typ = Convert.ToInt32(type);
            p.Tresc = HttpUtility.UrlDecode(title, System.Text.Encoding.Default);
      
            JavaScriptSerializer ser = new JavaScriptSerializer();
            var dict = ser.Deserialize<Dictionary<string, string>>(props);

            List<PropOdpowiedz> odps = new List<PropOdpowiedz>();
            if (p.Typ == 3 || p.Typ == 4) { 
                foreach (var pair in dict)
                {
                    PropOdpowiedz po = new PropOdpowiedz();
                    po.PropTresc = pair.Value;
                    odps.Add(po);
                }
            }
            p.props = odps;
            
            string sessToken = (string)Session["pyt_token"];
            if(Session["pyt_token"] == null || !sessToken.Equals(token)) {
                Session["pyt"] = new List<Pytanie>();
                Session["pyt_token"] = token;
            }
            List<Pytanie> lista = (List<Pytanie>)Session["pyt"];
            lista.Add(p);
            Session["pyt"] = lista;
        }
	}
}