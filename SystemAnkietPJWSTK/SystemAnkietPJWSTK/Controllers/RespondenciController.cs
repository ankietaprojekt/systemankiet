﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SystemAnkietPJWSTK.DAL;
using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace SystemAnkietPJWSTK.Controllers
{
    [CustomAuthorizeAttribute]
    public class RespondenciController : Controller
    {

        private IWydzialDbService dbWydzial = new WydzialDbContext();
        private IStudiaDbService dbStudia = new StudiaDbContext();
        private IGrupaStudenckaDbService dbGrupa = new GrupaStudenckaDbContext();
        private IRespondentDbService dbRespondent = new RespondentDbContext();
        private ISemestrDbService dbSemestr = new SemestrDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetWydzialy([DataSourceRequest]DataSourceRequest request)
        {
            IEnumerable<Wydzial> wydzialy = dbWydzial.SelectWydzialy();
            DataSourceResult result = wydzialy.ToDataSourceResult(request);
            return Json(result);
        }

        public ActionResult GetStudia(int idWydzial, [DataSourceRequest]DataSourceRequest request)
        {
            IEnumerable<Studia> studia = dbStudia.SelectStudia();
            studia = studia.Where(s => s.IdWydzial == idWydzial);
            DataSourceResult result = studia.ToDataSourceResult(request);
            return Json(result);
        }

        public ActionResult GetGrupy(int idStudia, string nrSemestru, [DataSourceRequest]DataSourceRequest request)
        {
            IEnumerable<GrupaStudencka> grupy = dbGrupa.GetGrupaStudenckaForCurrentSemester();
            grupy = grupy.Where(g => g.IdStudia == idStudia);
            DataSourceResult result = null;

            if (nrSemestru.Equals("ITN"))
            {
                grupy = grupy.Where(g => g.ITN == "1");
                result = grupy.ToDataSourceResult(request);
            }
            else
            {
                int nrSemestruu = int.Parse(nrSemestru.ToString());
                grupy = grupy.Where(g => g.NrSemestru == nrSemestruu);
                result = grupy.ToDataSourceResult(request);
            }
            result = grupy.ToDataSourceResult(request);
            return Json(result);
        }

        public ActionResult GetOsoby([DataSourceRequest]DataSourceRequest request)
        {
            IEnumerable<Osoba> osoby = dbRespondent.GetAllRespondents();
            DataSourceResult result = osoby.ToDataSourceResult(request);
            return Json(result);
        }

        public ActionResult GetWybrani([DataSourceRequest]DataSourceRequest request)
        {
            IEnumerable<Wybrany> osoby;
            if (Session["wybrani"] != null)
            {
                osoby = (List<Wybrany>)Session["wybrani"];
            }
            else
            {
                osoby = new List<Wybrany>();
            }
            DataSourceResult result = osoby.ToDataSourceResult(request);
            return Json(result);
        }

        public ActionResult GetSemestry(int idStudia, [DataSourceRequest]DataSourceRequest request)
        {
            List<Semestr> semestry = (List<Semestr>)dbSemestr.GetActiveSemestrNumbersByStudia(idStudia);

            Semestr itn = new Semestr
            {
                NrSemestru = "ITN", 
                IdStudia = idStudia
            };
            semestry.Add(itn);

            DataSourceResult result = semestry.ToDataSourceResult(request);
            return Json(result);
        }

        [HttpPost]
        public void Wybrani(List<Wybrany> wybrani)
        {
            if (wybrani != null)
            {
                Session["wybrani"] = wybrani;
            }
        }
    }
}