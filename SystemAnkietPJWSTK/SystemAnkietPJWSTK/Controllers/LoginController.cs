﻿using System.Web.Mvc;
using SystemAnkietPJWSTK.Models;

namespace SystemAnkietPJWSTK.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "Logowanie";

            Session["logged"] = false;

            return View();
        }

        [HttpPost]
        public ActionResult Index(Login log)
        {
            if (ModelState.IsValid)
            {
                if (log.userName.Equals("creator") && log.userPass.Equals("pass"))
                {
                    log.tworca = true;
                    log.userID = 1;
                    log.userPass = null;
                    Login(log);

                }
                else if (log.userName.Equals("cuser"))
                {
                    log.tworca = false;
                    log.userID = int.Parse(log.userPass);
                    log.userPass = null;
                    Login(log);
                }
                else
                {
                    return RedirectToAction("Error", "Login");
                }

                if (log.tworca)
                {
                    return RedirectToAction("Index", "Ankieta");
                }
                else
                {
                    return RedirectToAction("DoWypelnienia", "Ankieta");
                }
            }
            return View();
        }

        private void SetLogged(bool logged)
        {
            Session["logged"] = logged;
        }

        private void Login(Login log)
        {
            Session["userID"] = log.userID;
            Session["loggedUser"] = log;
            SetLogged(true);
        }

        public bool IsLogged()
        {
            return (bool)Session["logged"];
        }

        public bool Authorise()
        {
            if (System.Web.HttpContext.Current.Session["loggedUser"] == null || !(bool)System.Web.HttpContext.Current.Session["logged"])
            {
                RedirectToAction("Index", "Login");
                return false;
            }
            return true;
        }

        public ActionResult Logout()
        {
            Session["loggedUser"] = null;
            SetLogged(false);
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Error()
        {
            return View();
        }
	}
}