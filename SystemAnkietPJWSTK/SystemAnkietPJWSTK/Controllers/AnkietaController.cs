﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SystemAnkietPJWSTK.DAL;
using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using ActionParameterAlias;
using SystemAnkietPJWSTK.Utils;
using System.Net;

namespace SystemAnkietPJWSTK.Controllers
{
    [CustomAuthorizeAttribute]
    public class AnkietaController : Controller
    {
        private IAnkietaDbService dbAnkieta = new AnkietaDbContext();
        private IPytanieDbService dbPytanie = new PytanieDbContext();
        private IPropOdpowiedzDbService dbProp = new PropOdpowiedzDbContext();
        private IWydzialDbService dbWydzial = new WydzialDbContext();
        private IStudiaDbService dbStudia = new StudiaDbContext();
        private ISemestrDbService dbSemestr = new SemestrDbContext();
        private IGrupaStudenckaDbService dbGrupa = new GrupaStudenckaDbContext();
        private IGoscDbService dbGosc = new GoscDbContext();
        private IRespondentDbService dbRespondent = new RespondentDbContext();
        private IOsobaDbService dbOs = new OsobaDbContext();
        
        public ActionResult Index()
        {
            Login log = (Login)Session["loggedUser"];
            int user = log.userID;
            List<Ankieta> ankiety = (List<Ankieta>)dbAnkieta.GetAnkietaByCreator(user);

            return View(ankiety);
        }

        public ActionResult Robocze()
        {
            Login log = (Login)Session["loggedUser"];
            int user = log.userID;
            List<Ankieta> ankiety = (List<Ankieta>)dbAnkieta.GetAnkietaRoboczaByCreator(user);

            return View(ankiety);
        }

        [HttpGet]
        [ParameterAlias("clearSession", "id", Order = 3)]
        public ActionResult Create(bool? clearSession)
        {
            if (clearSession == true)
            {
                Session["ank"] = null;
                Session["pyt"] = null;
                Session["wybrani"] = null;
                Session["editAnk"] = false;
            }

            if (Session["ank"] != null)
            {
                Ankieta tmp = (Ankieta)Session["ank"];
                tmp.Opis = WebUtility.HtmlDecode(tmp.Opis);
                tmp.TrescMaila = WebUtility.HtmlDecode(tmp.TrescMaila);
                return View(tmp);
            }
            else
                return View(new Ankieta());
        }

        [ParameterAlias("idWybranejAnkiety", "id", Order = 3)]
        public ActionResult Edit(int idWybranejAnkiety)
        {
            LoadAnkietaToSession(idWybranejAnkiety);
            Session["editAnk"] = true;

            return RedirectToAction("Create", "Ankieta");
        }

        [HttpPost]
        public ActionResult Create(Ankieta ankieta)
        {
            if (ModelState.IsValid)
            {
                Login log = (Login)Session["loggedUser"];
                ankieta.IdOsoba = log.userID;
                if (ankieta.CzyPrzypomnienie == false)
                {
                    ankieta.CzasPrzyp = null;
                }

                Session["ank"] = ankieta;
                return RedirectToAction("NowePytanie", "Pytanie");
            }
            return View();
        }

        [HttpPost]
        public ActionResult SaveRobocza(Ankieta ankieta)
        {
            if (ModelState.IsValid)
            {
                Login log = (Login)Session["loggedUser"];
                ankieta.IdOsoba = log.userID;
                if (ankieta.CzyPrzypomnienie == false)
                {
                    ankieta.CzasPrzyp = null;
                }

                Session["ank"] = ankieta;
                SaveAnkieta(true);
            }
            return RedirectToAction("Robocze", "Ankieta");
        }

        public ActionResult PresubmitPreview()
        {
            List<Wybrany> wybrani = (List<Wybrany>)Session["wybrani"];

            foreach (Wybrany wyb in wybrani)
            {
                if (wyb.Typ.Equals("Semestr") && !wyb.Nazwa.Contains("("))
                {
                    wyb.Nazwa = wyb.Nazwa + " (" + dbStudia.GetStudiaByID(wyb.ParentID).Nazwa + ")";
                }
            }

            Session["wybrani"] = wybrani;

            return View();
        }

        [ParameterAlias("robocza", "id", Order = 3)]
        public ActionResult SaveAnkieta(bool? robocza)
        {
            Ankieta ankieta = (Ankieta)Session["ank"];
            if (ankieta == null)
            {
                if (robocza == true)
                {
                    ankieta = new Ankieta();
                }
                else
                {
                    return View("Index");
                }
            }

            int ankietaId;
            Login log = (Login)Session["loggedUser"];
            ankieta.IdOsoba = log.userID;
            if (robocza == true)
                ankieta.Status = "Robocza";
            else if (ankieta.DataRozp == DateTime.Today)
                ankieta.Status = "Uruchomiona";
            else
                ankieta.Status = "Utworzona";

            if (ankieta.Opis == null)
                ankieta.Opis = "";
            else
                ankieta.Opis = HttpUtility.UrlDecode(ankieta.Opis, System.Text.Encoding.Default);

            if (ankieta.TrescMaila == null)
                ankieta.TrescMaila = "";
            else
                ankieta.TrescMaila = HttpUtility.UrlDecode(ankieta.TrescMaila, System.Text.Encoding.Default);

            if (ankieta.CzyPrzypomnienie == false)
            {
                ankieta.CzasPrzyp = null;
            }


            if (Session["editAnk"] != null && (bool)Session["editAnk"])
            {
                ankietaId = ankieta.IdAnkieta;

                int updateCount = dbAnkieta.UpdateAnkieta(ankieta);
                int pytanieCount = dbPytanie.DeletePytanieByAnkieta(ankieta.IdAnkieta);
                int respCount = dbRespondent.DeleteAllRespondentsByAnkieta(ankieta.IdAnkieta);
            }
            else
            {
                ankietaId = dbAnkieta.InsertAnkieta(ankieta);
                ankieta.IdAnkieta = ankietaId;
            }

            List<Pytanie> pytania = (List<Pytanie>)Session["pyt"];
            if (pytania != null && pytania.Count > 0)
            {
                foreach (Pytanie p in pytania)
                {

                    p.IdAnkieta = ankietaId;
                    PytanieDbContext pDb = new PytanieDbContext();
                    int pytanieId = pDb.InsertPytanie(p);
                    foreach (PropOdpowiedz odp in p.props)
                    {
                        odp.IdPytanie = pytanieId;
                        PropOdpowiedzDbContext propDb = new PropOdpowiedzDbContext();
                        propDb.InsertPropOdp(odp);
                    }
                }
            }

            List<Wybrany> wybrani = (List<Wybrany>)Session["wybrani"];
            HashSet<Int32> respondenci = new HashSet<Int32>();
            List<String> goscie = new List<String>();
            IWydzialDbService wydzialDb = new WydzialDbContext();
            IGrupaStudenckaDbService grupaStudDb = new GrupaStudenckaDbContext();
            IStudiaDbService studiaDb = new StudiaDbContext();
            ISemestrDbService semDb = new SemestrDbContext();

            if (wybrani != null && wybrani.Count > 0)
            {
                foreach (Wybrany wyb in wybrani)
                {
                    if (wyb.Typ.Equals("Wydział"))
                    {
                        wydzialDb.AddWydzialToAnkieta(wyb.ID, ankieta);
                    }
                    else if (wyb.Typ.Equals("Studia"))
                    {
                        studiaDb.AddStudiaToAnkieta(wyb.ID, ankieta);
                    }
                    else if (wyb.Typ.Equals("Grupa"))
                    {
                        grupaStudDb.AddGrupaStudenckaToAnkieta(wyb.ID, ankieta);
                    }
                    else if (wyb.Typ.Equals("Semestr"))
                    {
                        if (wyb.Nazwa.Contains("ITN"))
                        {
                            List<Osoba> itn = (List<Osoba>)dbOs.GetOsobaITNByStudia(wyb.ParentID);
                            foreach (Osoba o in itn)
                            {
                                respondenci.Add(o.IdOsoba);
                            }
                        }
                        else
                        {
                            String numerSemestru = wyb.Nazwa.Substring(0, 1);
                            semDb.AddSemestrToAnkieta(Int32.Parse(numerSemestru), wyb.ParentID, ankieta);
                        }
                    }
                    else if (wyb.Typ.Equals("Osoba"))
                    {
                        respondenci.Add(wyb.ID);
                    }
                    else if (wyb.Typ.Equals("Gość"))
                    {
                        goscie.Add(wyb.Nazwa);
                    }
                }
            }

            IRespondentDbService respDb = new RespondentDbContext();
            if (respondenci != null && respondenci.Count > 0)
            {
                foreach (int idOsoba in respondenci)
                {
                    respDb.AddOsobaToAnkieta(idOsoba, ankieta);
                }
            }

            if (goscie != null && goscie.Count > 0)
            {
                foreach (String email in goscie)
                {
                    string link = GenerateGuestLink();
                    respDb.AddGoscToAnkieta(email, ankieta, link);
                }
            }

            if (Session["editAnk"] != null && (bool)Session["editAnk"])
            {
                Session["top_alert"] = new TopAlert(true, "Ankieta zostala zaktualizowana");
                Session["editAnk"] = false;
            }
            else
            {
                if (robocza == true)
                    Session["top_alert"] = new TopAlert(true, "Ankieta zostala zapisana w roboczych");
                else
                    Session["top_alert"] = new TopAlert(true, "Ankieta zostala zapisana");
            }

            return RedirectToAction("Index", "Ankieta");
        }

        public ActionResult GetAnkiety([DataSourceRequest]DataSourceRequest request)
        {
            Login log = (Login)Session["loggedUser"];
            int user = log.userID;
            IEnumerable<Ankieta> ankiety = dbAnkieta.GetAnkietaByCreator(user);
            DataSourceResult result = ankiety.ToDataSourceResult(request);
            System.Diagnostics.Debug.WriteLine("Weszło do Ankiety!");
            return Json(result);
        }

        public ActionResult GetRobocze([DataSourceRequest]DataSourceRequest request)
        {
            Login log = (Login)Session["loggedUser"];
            int user = log.userID;
            IEnumerable<Ankieta> ankiety = dbAnkieta.GetAnkietaRoboczaByCreator(user);
            DataSourceResult result = ankiety.ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateAnkiety([DataSourceRequest] DataSourceRequest request, Ankieta ankieta)
        {
            if (ankieta != null && ModelState.IsValid)
            {
                dbAnkieta.UpdateAnkieta(ankieta);
            }

            return Json(new[] { ankieta }.ToDataSourceResult(request, ModelState));
        }

        private String GenerateGuestLink()
        {
            String gStr = Guid.NewGuid().ToString();
            gStr = gStr.Replace("-", "");

            return gStr;
        }

        public ActionResult DoWypelnienia()
        {
            Login log = (Login)Session["loggedUser"];
            int user = log.userID;
            var ankiety = dbAnkieta.GetAnkietaByOsoba(user);
            return View(ankiety);
        }

        public ActionResult GetDoWypelnienia([DataSourceRequest]DataSourceRequest request)
        {
            Login log = (Login)Session["loggedUser"];
            int user = log.userID;
            List<Ankieta> ankiety = (List<Ankieta>)dbAnkieta.GetAnkietaByOsoba(user);

            DataSourceResult result = ankiety.ToDataSourceResult(request);

            Log.d("Ankiety count : " + ankiety.Count);
            return Json(result);
        }

        [ParameterAlias("idWybranejAnkiety", "id", Order = 3)]
        public ActionResult ShowPreview(int idWybranejAnkiety)
        {
            ViewData["notPermitted"] = true;

            Ankieta ank = null;
            Login log = (Login)Session["loggedUser"];

            if (log.tworca)
            {

                ank = dbAnkieta.GetAnkietaByID(idWybranejAnkiety);

                if (ank.IdOsoba == log.userID)
                {
                    var pytania = dbPytanie.GetPytanieByAnkieta(idWybranejAnkiety);

                    List<Pytanie> formularz = new List<Pytanie>();

                    foreach (Pytanie p in pytania)
                    {
                        if (p.IdAnkieta == idWybranejAnkiety)
                        {
                            p.props = (List<PropOdpowiedz>)dbProp.GetPropOdpByPytanie(p.Id);

                            formularz.Add(p);
                        }
                    }

                    ViewData["forumlarz"] = formularz;
                    ViewData["notPermitted"] = false;
                }
            }

            return View(ank);
        }

        [ParameterAlias("idAnkiety", "id", Order = 3)]
        public ActionResult CreateFromExisted(int idAnkiety)
        {

            LoadAnkietaToSession(idAnkiety);

            return RedirectToAction("Create", "Ankieta");

        }

        private void LoadAnkietaToSession(int idWybranejAnkiety)
        {
            Ankieta ankieta = dbAnkieta.GetAnkietaByID(idWybranejAnkiety);
            List<Pytanie> pytania = (List<Pytanie>)dbPytanie.GetPytanieByAnkieta(idWybranejAnkiety);
            foreach (Pytanie pyt in pytania)
                pyt.props = (List<PropOdpowiedz>)dbProp.GetPropOdpByPytanie(pyt.Id);

            List<Wybrany> wybrani = new List<Wybrany>();

            List<Wydzial> wydzialy = (List<Wydzial>)dbWydzial.GetWydzialByAnkieta(idWybranejAnkiety);
            List<Studia> studia = (List<Studia>)dbStudia.GetStudiaByAnkieta(idWybranejAnkiety);
            List<Semestr> semestry = (List<Semestr>)dbSemestr.GetSemestrByAnkieta(idWybranejAnkiety);
            List<GrupaStudencka> grupy = (List<GrupaStudencka>)dbGrupa.GetGrupaStudenckaByAnkieta(idWybranejAnkiety);
            List<Gosc> goscie = (List<Gosc>)dbGosc.GetGoscByAnkieta(idWybranejAnkiety);
            List<Osoba> osoby = (List<Osoba>)dbRespondent.GetRespondentByAnkieta(idWybranejAnkiety);

            foreach (Wydzial w in wydzialy)
            {
                Wybrany wyb = new Wybrany();

                wyb.Typ = "Wydział";
                wyb.Nazwa = w.Nazwa;
                wyb.ID = w.IdWydzial;

                wybrani.Add(wyb);
            }
            foreach (Studia s in studia)
            {
                Wybrany wyb = new Wybrany();

                wyb.Typ = "Studia";
                wyb.Nazwa = s.Nazwa;
                wyb.ID = s.IdStudia;

                wybrani.Add(wyb);

            }
            foreach (Semestr s in semestry)
            {
                Wybrany wyb = new Wybrany();

                wyb.Typ = "Semestr";
                wyb.Nazwa = s.NrSemestru;
                wyb.ParentID = s.IdStudia;

                wybrani.Add(wyb);

            }
            foreach (GrupaStudencka gr in grupy)
            {
                Wybrany wyb = new Wybrany();

                wyb.Typ = "Grupa";
                wyb.Nazwa = gr.NrGrupy;
                wyb.ID = gr.IdGrupy;

                wybrani.Add(wyb);

            }
            foreach (Gosc g in goscie)
            {
                Wybrany wyb = new Wybrany();

                wyb.Typ = "Gość";
                wyb.Nazwa = g.Email;
                wyb.ID = g.IdGosc;

                wybrani.Add(wyb);

            }
            foreach (Osoba o in osoby)
            {
                Wybrany wyb = new Wybrany();

                wyb.Typ = "Osoba";
                wyb.Nazwa = o.Imie + " " + o.Nazwisko;
                wyb.ID = o.IdOsoba;

                wybrani.Add(wyb);

            }

            Session["ank"] = ankieta;
            Session["pyt"] = pytania;
            Session["wybrani"] = wybrani;
        }

        [ParameterAlias("idAnkieta", "id", Order = 3)]
        public ActionResult StartStop(int idAnkieta, bool start)
        {
            Ankieta ank = dbAnkieta.GetAnkietaByID(idAnkieta);

            if (start && ank.Status.Equals("Utworzona"))
            {
                dbAnkieta.UruchomAnkiete(idAnkieta);
            }
            else if (!start && ank.Status.Equals("Uruchomiona"))
            {
                dbAnkieta.ZamknijAnkiete(idAnkieta);
            }

            return RedirectToAction("Index", "Ankieta");
        }
    }
}