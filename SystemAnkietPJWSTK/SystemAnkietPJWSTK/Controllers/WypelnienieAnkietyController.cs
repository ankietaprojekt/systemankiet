﻿using System.Collections.Generic;
using System.Web.Mvc;
using SystemAnkietPJWSTK.DAL;
using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using SystemAnkietPJWSTK.Utils;
using ActionParameterAlias;

namespace SystemAnkietPJWSTK.Controllers
{
    [CustomAuthorizeAttribute]
    public class WypelnienieAnkietyController : Controller
    {

        private IOdpowiedzDbService dbOdpowiedz = new OdpowiedzDbContext();
        private IPropOdpowiedzDbService dbPropOdpowiedz = new PropOdpowiedzDbContext();
        private IPytanieDbService dbPytanie = new PytanieDbContext();
        private IAnkietaDbService dbAnkieta = new AnkietaDbContext();
        private IWypelnienieDbService dbWypelnienie = new WypelnienieDbContext();

        [ParameterAlias("idWybranejAnkiety", "id", Order = 3)]
        public ActionResult WyswietlAnkiete(int idWybranejAnkiety)
        {
            Ankieta ank = dbAnkieta.GetAnkietaByID(idWybranejAnkiety);

            bool isExists = dbWypelnienie.IsUserWypelnienieExist(idWybranejAnkiety, (int)Session["userID"]);

            if (isExists && ank.CzyEdyt)
            {
                return RedirectToAction("EdytujAnkiete", "WypelnienieAnkiety", new { id = idWybranejAnkiety });
            }

            var pytania = dbPytanie.GetPytanieByAnkieta(idWybranejAnkiety);

            List<Pytanie> formularz = new List<Pytanie>();

            foreach (Pytanie p in pytania)
            {
                if (p.IdAnkieta == idWybranejAnkiety)
                {
                    p.props = (List<PropOdpowiedz>)dbPropOdpowiedz.GetPropOdpByPytanie(p.Id);

                    formularz.Add(p);
                }
            }

            ViewData["forumlarz"] = formularz;

            return View(ank);
        }

        [AllowAnonymous]
        [ParameterAlias("token", "id", Order = 3)]
        public ActionResult WyswietlAnkieteGosc(string token)
        {
            Ankieta ank = dbAnkieta.GetAnkietaByToken(token);

            if (ank != null)
            {
                Session["token"] = token;
            }
            else
            {
                return View("BrakAnkiety");
            }


            bool isExists = dbAnkieta.IsGuestWypelnienieExists(ank.IdAnkieta, token);

            if (isExists && ank.CzyEdyt)
            {
                return RedirectToAction("EdytujAnkieteGosc", "WypelnienieAnkiety", new { id = token });
            }

            var pytania = dbPytanie.GetPytanieByAnkieta(ank.IdAnkieta);

            List<Pytanie> formularz = new List<Pytanie>();

            foreach (Pytanie p in pytania)
            {
                if (p.IdAnkieta == ank.IdAnkieta)
                {
                    p.props = (List<PropOdpowiedz>)dbPropOdpowiedz.GetPropOdpByPytanie(p.Id);

                    formularz.Add(p);
                }
            }

            ViewData["forumlarz"] = formularz;

            return View(ank);
        }


        public void WyswietlAnkiety(int id, List<List<string>> odp)
        {
            Ankieta ank = dbAnkieta.GetAnkietaByID(id);
            Login log = (Login)Session["loggedUser"];

            bool isExists = dbWypelnienie.IsUserWypelnienieExist(id, log.userID);

            if (isExists && ank.CzyEdyt)
            {
                int idWyp = dbWypelnienie.GetWypelnienieByUserAnkieta(log.userID, id);

                if (idWyp == -1)
                {
                    RedirectToAction("Error", "Login");
                }

                dbWypelnienie.UpdateWypelnienie(idWyp);

                dbOdpowiedz.DeleteOdpowiedzZamknietaByWypelnienie(idWyp);

                for (int i = 0; i < odp.Count; i++)
                {
                    if (odp[i][1] != null)
                    {
                        dbOdpowiedz.UpdateOdpowiedzOtwarta(idWyp, int.Parse(odp[i][0]), odp[i][1]);
                    }
                    else
                    {
                        dbOdpowiedz.AddOdpowiedzZamknieta(idWyp, int.Parse(odp[i][0]));
                    }
                }

                Session["top_alert"] = new TopAlert(true, "Twoje odpowiedzi zostały zaktualizowane");

                RedirectToAction("DoWypelnienia", "Ankieta");

            }
            else
            {

                int wypelnienieId = dbWypelnienie.InsertWypelnienie(id);

                for (int i = 0; i < odp.Count; i++)
                {
                    if (odp[i][1] != null)
                    {
                        dbOdpowiedz.AddOdpowiedzOtwarta(wypelnienieId, int.Parse(odp[i][0]), odp[i][1]);
                    }
                    else
                    {
                        dbOdpowiedz.AddOdpowiedzZamknieta(wypelnienieId, int.Parse(odp[i][0]));
                    }
                }

                dbWypelnienie.InsertWypUser(wypelnienieId, log.userID);
            }

            Session["top_alert"] = new TopAlert(true, "Twoje odpowiedzi zostały zapisane");
        }

        [AllowAnonymous]
        public ActionResult Potwierdzenie()
        {
            return View();
        }

        [ParameterAlias("idWybranejAnkiety", "id", Order = 3)]
        public ActionResult EdytujAnkiete(int idWybranejAnkiety)
        {
            Ankieta ank = dbAnkieta.GetAnkietaByID(idWybranejAnkiety);
            Login log = (Login)Session["loggedUser"];

            var pytania = dbPytanie.GetPytanieByAnkieta(idWybranejAnkiety);

            List<Pytanie> formularz = new List<Pytanie>();

            foreach (Pytanie p in pytania)
            {
                if (p.IdAnkieta == idWybranejAnkiety)
                {
                    p.props = (List<PropOdpowiedz>)dbPropOdpowiedz.GetPropOdpByPytanie(p.Id);

                    formularz.Add(p);
                }
            }

            ViewData["forumlarz"] = formularz;

            List<Odpowiedz> odpowiedzi = (List<Odpowiedz>)dbOdpowiedz.GetOdpowiedzByUserAnkieta(log.userID, idWybranejAnkiety);

            ViewData["odpowiedzi"] = odpowiedzi;

            return View(ank);
        }

        [AllowAnonymous]
        [ParameterAlias("token", "id", Order = 3)]
        public ActionResult EdytujAnkieteGosc(string token)
        {
            Ankieta ank = dbAnkieta.GetAnkietaByToken(token);

            if (ank != null)
                Session["token"] = token;

            int idWybranejAnkiety = ank.IdAnkieta;

            var pytania = dbPytanie.GetPytanieByAnkieta(idWybranejAnkiety);

            List<Pytanie> formularz = new List<Pytanie>();

            foreach (Pytanie p in pytania)
            {
                if (p.IdAnkieta == idWybranejAnkiety)
                {
                    p.props = (List<PropOdpowiedz>)dbPropOdpowiedz.GetPropOdpByPytanie(p.Id);

                    formularz.Add(p);
                }
            }

            ViewData["forumlarz"] = formularz;

            List<Odpowiedz> odpowiedzi = (List<Odpowiedz>)dbOdpowiedz.GetOdpowiedzByGuestAnkieta(token, idWybranejAnkiety);

            ViewData["odpowiedzi"] = odpowiedzi;

            return View(ank);
        }

        [AllowAnonymous]
        public void ZapiszAnkieteGosc(int id, List<List<string>> odp)
        {

            Ankieta ank = dbAnkieta.GetAnkietaByToken(Session["token"].ToString());

            bool isExists = dbAnkieta.IsGuestWypelnienieExists(id, Session["token"].ToString());

            if (isExists && ank.CzyEdyt)
            {
                int idWyp = dbWypelnienie.GetWypelnienieByGuestAnkieta(Session["token"].ToString(), id);

                if (idWyp == -1)
                {
                    RedirectToAction("Error", "Login");
                }

                dbWypelnienie.UpdateWypelnienie(idWyp);

                dbOdpowiedz.DeleteOdpowiedzZamknietaByWypelnienie(idWyp);

                for (int i = 0; i < odp.Count; i++)
                {
                    if (odp[i][1] != null)
                    {
                        dbOdpowiedz.UpdateOdpowiedzOtwarta(idWyp, int.Parse(odp[i][0]), odp[i][1]);
                    }
                    else
                    {
                        dbOdpowiedz.AddOdpowiedzZamknieta(idWyp, int.Parse(odp[i][0]));
                    }
                }

                Session["top_alert"] = new TopAlert(true, "Twoje odpowiedzi zostały zaktualizowane");
            }
            else
            {
                int wypelnienieId = dbWypelnienie.InsertWypelnienie(id);

                for (int i = 0; i < odp.Count; i++)
                {
                    if (odp[i][1] != null)
                    {
                        dbOdpowiedz.AddOdpowiedzOtwarta(wypelnienieId, int.Parse(odp[i][0]), odp[i][1]);
                        System.Diagnostics.Debug.WriteLine(odp[i][0] + " " + odp[i][1]);
                    }
                    else
                    {
                        dbOdpowiedz.AddOdpowiedzZamknieta(wypelnienieId, int.Parse(odp[i][0]));
                        System.Diagnostics.Debug.WriteLine(odp[i][0] + " " + odp[i][1]);
                    }
                }

                dbWypelnienie.InsertWypGosc(wypelnienieId, (string)Session["token"]);

                Session["top_alert"] = new TopAlert(true, "Twoje odpowiedzi zostały zapisane");
            }
        }
    }
}