﻿using ActionParameterAlias;
using Kendo.Mvc.UI;
using System.Collections.Generic;
using System.Web.Mvc;
using SystemAnkietPJWSTK.DAL;
using SystemAnkietPJWSTK.DAL.Interfaces;
using SystemAnkietPJWSTK.Models;
using Kendo.Mvc.Extensions;
using SystemAnkietPJWSTK.Utils;
using System.IO;

namespace SystemAnkietPJWSTK.Controllers
{
    [CustomAuthorizeAttribute]
    public class RaportController : Controller
    {
        private IAnkietaDbService dbAnkieta = new AnkietaDbContext();
        private IRespondentDbService dbResp = new RespondentDbContext();
        private IPytanieDbService dbPytanie = new PytanieDbContext();
        private IPropOdpowiedzDbService dbProp = new PropOdpowiedzDbContext();
        private IOdpowiedzDbService dbOdp = new OdpowiedzDbContext();

        [ParameterAlias("idAnkieta", "id", Order = 3)]
        public ActionResult Index(int idAnkieta)
        {
            Ankieta ank = dbAnkieta.GetAnkietaByID(idAnkieta);

            ViewData["ankieta"] = ank;

            List<Pytanie> pytania = (List<Pytanie>) dbPytanie.GetPytanieByAnkieta(idAnkieta);

            foreach (Pytanie p in pytania)
            {
                p.props = (List<PropOdpowiedz>)dbProp.GetPropOdpByPytanie(p.Id);
                p.odpowiedzi = (List<Odpowiedz>)dbOdp.GetOdpowiedzByPytanie(p.Id);
            }

            ViewData["pytania"] = pytania;

            return View();
        }

        public ActionResult Respondents_Read([DataSourceRequest]DataSourceRequest request, int idAnkieta)
        {
            List<RespondentDoRaportu> allRespondents = (List<RespondentDoRaportu>)dbResp.GetRespondentForRaportByAnkieta(idAnkieta);
            return Json(allRespondents.ToDataSourceResult(request));
        }

        private Dictionary<int, string> resolutionColors = new Dictionary<int, string>() { 
            {3,"#FF0000"},
            {6,"#FF00EA"},
            {1, "#0D00FF"},
            {4, "#00C8FF"},
            {5, "#15FF00"},
            {2, "#FFFF00"},
            {7, "#FF6A00"},
            {8, "#912EC9"},
            {9, "#2E93C9"},
            {10, "#C92E74"}
        };

        public ActionResult GetStatystykiByPytanieTypAnkieta([DataSourceRequest]DataSourceRequest request, int idPytanie, int idAnkieta, int typPytania)
        {
            List<Odpowiedz> odpowiedzi = (List<Odpowiedz>)dbOdp.GetOdpowiedzByPytanie(idPytanie);
            List<PropOdpowiedz> propsy = (List<PropOdpowiedz>)dbProp.GetPropOdpByPytanie(idPytanie);  

            var viewModel = new List<ViewModel>();

            int i = 1;

            if (typPytania == 3 || typPytania == 4)
            {
                foreach (PropOdpowiedz prop in propsy)
                {
                    ViewModel model = new ViewModel();
                    
                    int all = odpowiedzi.Count;
                    int size = 0;

                    foreach (Odpowiedz odp in odpowiedzi)
                    {
                        if (odp.IdPole == prop.IdPole)
                        {
                            size++;
                        }
                    }

                    model.Percentage = (double)size / (double)all;

                    model.Value = prop.PropTresc + " - " + model.Percentage.ToString("0.##%") + " (" + size + ")";
                    model.Color = resolutionColors[i];

                    viewModel.Add(model);

                    i++;
                }
            }

            return Json(viewModel);
        }

        [HttpGet]
        public ActionResult PreviewByRespondent(int idAnkieta, int idResp, bool isGuest)
        {

            Log.d("Weszło do preview!");

            Ankieta ank = dbAnkieta.GetAnkietaByID(idAnkieta);

            var pytania = dbPytanie.GetPytanieByAnkieta(idAnkieta);

            List<Pytanie> formularz = new List<Pytanie>();

            foreach (Pytanie p in pytania)
            {
                if (p.IdAnkieta == idAnkieta)
                {
                    p.props = (List<PropOdpowiedz>)dbProp.GetPropOdpByPytanie(p.Id);

                    formularz.Add(p);
                }
            }

            ViewData["forumlarz"] = formularz;

            List<Odpowiedz> odpowiedzi;
            if(isGuest) {

                odpowiedzi = (List<Odpowiedz>)dbOdp.GetOdpowiedzByGuestAnkieta(idResp, idAnkieta);
            } else {
                odpowiedzi =  (List<Odpowiedz>)dbOdp.GetOdpowiedzByUserAnkieta(idResp, idAnkieta);
            }


            ViewData["odpowiedzi"] = odpowiedzi;

            return PartialView("PreviewByRespondent", ank);
        }

        [HttpGet]
        [ParameterAlias("idAnkieta", "id", Order = 3)]
        public ActionResult Download(int idAnkieta)
        {
            MemoryStream memStream = ExportToExcel.CreateReport(idAnkieta);

            Ankieta ankieta = dbAnkieta.GetAnkietaByID(idAnkieta);

            string nazwaAnkiety = "Ankieta_Raport.xlsx";

            if (ankieta != null)
            {
                nazwaAnkiety = ankieta.Nazwa + "_Raport.xlsx";
            }

            memStream.Position = 0;
            
            return File(memStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", nazwaAnkiety);
        }
    }
}